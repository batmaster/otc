<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationDota2sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registration_dota2s', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->increments('id', true, true);

            $table->string('team');
            $table->string('image_team', 1024)->nullable();
            $table->string('image_logo', 1024)->nullable();

            $table->string('image_receipt')->nullable();
            $table->boolean('approved')->default(false);

            $table->string('player1_name');
            $table->integer('player1_age');
            $table->string('player1_steam', 64);
            $table->string('player1_phone', 32);
            $table->string('player1_line');
            $table->string('player1_fb', 64);
            $table->string('player1_email', 64);
            $table->string('player1_id');

            $table->string('player2_name');
            $table->integer('player2_age');
            $table->string('player2_steam', 64);
            $table->string('player2_id');

            $table->string('player3_name');
            $table->integer('player3_age');
            $table->string('player3_steam', 64);
            $table->string('player3_id');

            $table->string('player4_name');
            $table->integer('player4_age');
            $table->string('player4_steam', 64);
            $table->string('player4_id');

            $table->string('player5_name');
            $table->integer('player5_age');
            $table->string('player5_steam', 64);
            $table->string('player5_id');

            $table->string('player6_name')->nullable();
            $table->integer('player6_age')->nullable();
            $table->string('player6_steam', 64)->nullable();
            $table->string('player6_id')->nullable();

            $table->string('player7_name')->nullable();
            $table->integer('player7_age')->nullable();
            $table->string('player7_steam', 64)->nullable();
            $table->string('player7_id')->nullable();

            $table->string('player8_name')->nullable();
            $table->integer('player8_age')->nullable();
            $table->string('player8_steam', 64)->nullable();
            $table->string('player8_id')->nullable();

            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registration_dota2s');
    }
}
