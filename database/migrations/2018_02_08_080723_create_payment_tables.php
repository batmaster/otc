<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id')->index();

            $table->string('ref');
            $table->double('amount');
            $table->string('datetime');
            $table->string('to_bank');
            $table->string('from_bank');
            $table->string('from_bank_branch');
            $table->string('image_confirm');
            $table->string('note')->nullable();

            $table->boolean('confirmed')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
