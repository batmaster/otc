<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistration22sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registration22s', function (Blueprint $table) {
            $table->increments('id')->index();

            $table->string('school');
            $table->string('team');
            $table->string('image_team')->nullable();
            $table->string('image_logo')->nullable();

            $table->string('player1_name');
            $table->string('player1_phone');
            $table->string('player1_line');
            $table->string('player1_fb');
            $table->string('player1_email');
            $table->string('player1_gender');
            $table->string('player1_grade');
            $table->string('player1_id');

            $table->string('player2_name');
            $table->string('player2_phone');
            $table->string('player2_fb');
            $table->string('player2_email');
            $table->string('player2_gender');
            $table->string('player2_grade');
            $table->string('player2_id');

            $table->string('player3_name');
            $table->string('player3_phone');
            $table->string('player3_fb');
            $table->string('player3_email');
            $table->string('player3_gender');
            $table->string('player3_grade');
            $table->string('player3_id');

            $table->string('player4_name');
            $table->string('player4_phone');
            $table->string('player4_fb');
            $table->string('player4_email');
            $table->string('player4_gender');
            $table->string('player4_grade');
            $table->string('player4_id');

            $table->string('player5_name');
            $table->string('player5_phone');
            $table->string('player5_fb');
            $table->string('player5_email');
            $table->string('player5_gender');
            $table->string('player5_grade');
            $table->string('player5_id');

            $table->string('player6_name')->nullable();
            $table->string('player6_phone')->nullable();
            $table->string('player6_fb')->nullable();
            $table->string('player6_email')->nullable();
            $table->string('player6_gender')->nullable();
            $table->string('player6_grade')->nullable();
            $table->string('player6_id')->nullable();

            $table->string('player7_name')->nullable();
            $table->string('player7_phone')->nullable();
            $table->string('player7_fb')->nullable();
            $table->string('player7_email')->nullable();
            $table->string('player7_gender')->nullable();
            $table->string('player7_grade')->nullable();
            $table->string('player7_id')->nullable();

            $table->string('player8_name')->nullable();
            $table->string('player8_phone')->nullable();
            $table->string('player8_fb')->nullable();
            $table->string('player8_email')->nullable();
            $table->string('player8_gender')->nullable();
            $table->string('player8_grade')->nullable();
            $table->string('player8_id')->nullable();

            $table->string('ticket_code')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registration22s');
    }
}
