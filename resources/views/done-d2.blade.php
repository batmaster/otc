@extends('layouts.layout')

@section('title', 'OTW To The Champion Dota2 | Registration')

@section('style')

@endsection

@section('content')
<div style="background-image: url('{{ asset('assets/images/Dot1.jpg')}}'); background-repeat: no-repeat; background-size: cover; background-color: #D6D0E1; padding-top: 10%; padding-bottom: 20px">

    <div class="container" style="background: white; padding: 40; max-width: 800;position: relative; left: 0; top: 0; box-shadow: 0px -10px #C52431;">
            <h2>ON THE WAY TO THE CHAMPION DOTA2</h2>
            <br>
            <center><h3 style="color: #C52431;"><i>สมัครสำเร็จแล้ว<i></h3></center>
            <br>

            <p><h5>*โปรดติดตามรายละเอียดการแข่งขันได้ที่เพจ</h5>
                Facebook : <b><a href="https://www.facebook.com/OTWDota2Esports">OTWDota2Esports</a></b><br>
                Line : <b>@otwjuniorleague</b> (มี@ด้วย)
            </p>

            <img src="{{ asset('assets/images/Poster_PART2_.png')}}" style="width: 100%; display: none"></img>


            <h3 style="margin-top: 50px; background-color: #9C222F; padding: 20; color: white">
                เปิดรับสมัครแล้วตั้งแต่วันนี้ - 31 มีนาคม 2561 เวลา 23.59 น.
            </h3>

            </form>

        <script>

        $("#button-register").click(function() {
            window.location = '{{url('register')}}';
        });

        </script>
    </div>
</div>
@endsection
