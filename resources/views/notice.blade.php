@extends('layouts.layout')

@section('title', 'OTW To The Champion | Notice')

@section('style')
    <style>
        body {
            background-image: url('{{ asset('assets/images/bg.jpg')}}');
            background-size: cover;
            background-repeat: repeat;
            background-position: center center;
        }
        h3 {
            color: #f2aa3a;
            background-color: #2b333dee;
            padding: 20px;
            /* text-shadow: 1px 0px 5px #f2aa3a66; */
        }
        p {
            color: #f2aa3a;
            padding: 20px;
            font-size: 18px;
            /* text-shadow: 1px 0px 15px #f2aa3a66; */
            text-indent: 50px;
        }

    </style>
@endsection


@section('content')
<div class="container" >
    <div style="background-color: #353e4899; min-height: 80%; box-shadow: 1px 1px 20px;">
        <h3>เปิดรับสมัครการแข่งขัน OTW To The Champion 2018 (Part 2)!!!</h3>
        <p>เปิดโอกาสให้น้องๆ ระดับมัธยมศึกษาหรือเทียบเท่าที่มีอายุไม่เกิน 18 ปี และบุคคลทั่วไปไม่จำกัดอายุ มาเข้าร่วมการแข่งขันเกม Rov เพื่อค้นหาแชมป์เพียงหนึ่งเดียวในเขตพื้นที่กรุงเทพมหานครและปริมณฑล พร้อมชิงรางวัลมูลค่ารวมกว่า 1,000,000 บาท!!! และได้เซ็นต์สัญญาเป็นนักกีฬา eSports อย่างเต็มตัว ภายใต้บริษัท บีเคเค ดีเวลลอปเปอร์ จำกัด 1 ปี พร้อมเงินเดือน 20,000 บาท ต่อคน!!! ซึ่ง Part 2 นี้เปิดรับสมัครถึง 1024 ทีม !!!
        </p>
    </div>
</div>

    <script>
        $("#button-register").click(function() {
            window.location = '{{url('register')}}';
        });
    </script>
@endsection
