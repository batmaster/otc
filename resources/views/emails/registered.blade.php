@extends('emails.layout')

@section('title', 'ได้รับข้อมูลการสมัครแล้ว')

@section('content')


    <img style="width: 100%" src="<?php echo $message->embed(resource_path('img/email/RoVPart2_cover_otc123132.jpg')); ?>">

    <div class="row breadcrumb my-4">
        <span style="margin: auto; font-weight: bold;">
            Ticket Code:
            <span style="font-weight: normal;">
                {{ $registration22->ticket_code }}
            </span>
        </span>
        <span style="margin: auto; font-weight: bold;">
            วันเวลาที่สมัคร :
            <span style="font-weight: normal;">
                {{ $registration22->created_at }}
            </span>
        </span>
    </div>

    <div class="row mb-2">
        <div class="col-md-2">
            <b>ชื่อโรงเรียน</b>
        </div>
        <div class="col-md-4">
            <span>{{ $registration22->school }}</span>
        </div>
        <div class="col-md-2">
            <b>ชื่อทีม</b>
        </div>
        <div class="col-md-4">
            <span>{{ $registration22->team }}</span>
        </div>
    </div>

    <table id="table" class="table table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>สมาชิก</th>
                <th>ชื่อ - นามสกุล</th>
                <th>เบอร์โทรศัพท์</th>
                <th>LINE ID</th>
                <th>Facebook</th>
                <th>Email</th>
                <th>เพศ</th>
                <th>ระดับชั้น</th>
                <th>รหัสประจำตัวนักเรียน</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>สมาชิกคนที่ 1 (หัวหน้าทีม)</td>
                <td>{{ $registration22->player1_name }}</td>
                <td>{{ $registration22->player1_phone }}</td>
                <td>{{ $registration22->player1_line }}</td>
                <td>{{ $registration22->player1_fb }}</td>
                <td>{{ $registration22->player1_email }}</td>
                <td>{{ $registration22->player1_gender == 'male' ? 'ชาย' : 'หญิง' }}</td>
                <td>{{ "ม." . $registration22->player1_grade }}</td>
                <td>{{ $registration22->player1_id }}</td>
            </tr>
            <tr>
                <td>สมาชิกคนที่ 2</td>
                <td>{{ $registration22->player2_name }}</td>
                <td>{{ $registration22->player2_phone }}</td>
                <td></td>
                <td>{{ $registration22->player2_fb }}</td>
                <td>{{ $registration22->player2_email }}</td>
                <td>{{ $registration22->player2_gender == 'male' ? 'ชาย' : 'หญิง' }}</td>
                <td>{{ "ม." . $registration22->player2_grade }}</td>
                <td>{{ $registration22->player2_id }}</td>
            </tr>
            <tr>
                <td>สมาชิกคนที่ 3</td>
                <td>{{ $registration22->player2_name }}</td>
                <td>{{ $registration22->player2_phone }}</td>
                <td></td>
                <td>{{ $registration22->player3_fb }}</td>
                <td>{{ $registration22->player3_email }}</td>
                <td>{{ $registration22->player3_gender == 'male' ? 'ชาย' : 'หญิง' }}</td>
                <td>{{ "ม." . $registration22->player3_grade }}</td>
                <td>{{ $registration22->player3_id }}</td>
            </tr>
            <tr>
                <td>สมาชิกคนที่ 4</td>
                <td>{{ $registration22->player4_name }}</td>
                <td>{{ $registration22->player4_phone }}</td>
                <td></td>
                <td>{{ $registration22->player4_fb }}</td>
                <td>{{ $registration22->player4_email }}</td>
                <td>{{ $registration22->player4_gender == 'male' ? 'ชาย' : 'หญิง' }}</td>
                <td>{{ "ม." . $registration22->player4_grade }}</td>
                <td>{{ $registration22->player4_id }}</td>
            </tr>
            <tr>
                <td>สมาชิกคนที่ 5</td>
                <td>{{ $registration22->player5_name }}</td>
                <td>{{ $registration22->player5_phone }}</td>
                <td></td>
                <td>{{ $registration22->player5_fb }}</td>
                <td>{{ $registration22->player5_email }}</td>
                <td>{{ $registration22->player5_gender == 'male' ? 'ชาย' : 'หญิง' }}</td>
                <td>{{ "ม." . $registration22->player5_grade }}</td>
                <td>{{ $registration22->player5_id }}</td>
            </tr>
            <tr>
                <td>สมาชิกสำรองคนที่ 1</td>
                <td>{{ $registration22->player6_name }}</td>
                <td>{{ $registration22->player6_name != "" ? $registration22->player6_phone : "" }}</td>
                <td></td>
                <td>{{ $registration22->player6_name != "" ? $registration22->player6_fb : "" }}</td>
                <td>{{ $registration22->player6_name != "" ? $registration22->player6_email : "" }}</td>
                <td>{{ $registration22->player6_name != "" ? ($registration22->player6_gender == 'male' ? 'ชาย' : 'หญิง') : "" }}</td>
                <td>{{ $registration22->player6_name != "" ? "ม." . $registration22->player6_grade : "" }}</td>
                <td>{{ $registration22->player6_name != "" ? $registration22->player6_id : "" }}</td>
            </tr>
            <tr>
                <td>สมาชิกสำรองคนที่ 2</td>
                <td>{{ $registration22->player7_name }}</td>
                <td>{{ $registration22->player7_name != "" ? $registration22->player7_phone : ""}}</td>
                <td></td>
                <td>{{ $registration22->player7_name != "" ? $registration22->player7_fb : "" }}</td>
                <td>{{ $registration22->player7_name != "" ? $registration22->player7_email : "" }}</td>
                <td>{{ $registration22->player7_name != "" ? ($registration22->player7_gender == 'male' ? 'ชาย' : 'หญิง') : "" }}</td>
                <td>{{ $registration22->player7_name != "" ? "ม." . $registration22->player7_grade : "" }}</td>
                <td>{{ $registration22->player7_name != "" ? $registration22->player7_id : "" }}</td>
            </tr>
            <tr>
                <td>สมาชิกสำรองคนที่ 3</td>
                <td>{{ $registration22->player8_name }}</td>
                <td>{{ $registration22->player8_name != "" ? $registration22->player8_phone : "" }}</td>
                <td></td>
                <td>{{ $registration22->player8_name != "" ? $registration22->player8_fb : "" }}</td>
                <td>{{ $registration22->player8_name != "" ? $registration22->player8_email : ""}}</td>
                <td>{{ $registration22->player8_name != "" ? ($registration22->player8_gender == 'male' ? 'ชาย' : 'หญิง') : "" }}</td>
                <td>{{ $registration22->player8_name != "" ? "ม." . $registration22->player8_grade : "" }}</td>
                <td>{{ $registration22->player8_name != "" ? $registration22->player8_id : "" }}</td>
            </tr>
        </tbody>
    </table>

    <b>ใบเสร็จรับเงินสำหรับใบสั่งซื้อ REC{{ $registration22->receipt }}</b>
    <p>
        <table>
            <tr>
                <td>ยอดเงินค่าสมัครสำหรับ</td>
                <td>{{ $registration22->people }} คน</td>
            </tr>
            <tr>
                <td><b>ยอดรวม</b></td>
                <td>{{ $registration22->people * 200 }} บาท</td>
            </tr>
            <tr>
                <td>ราคาสินค้า</td>
                <td>{{ $registration22->people * 200 * 0.93 }} บาท</td>
            </tr>
            <tr>
                <td>VAT 7%</td>
                <td>{{ $registration22->people * 200 * 0.07 }} บาท</td>
            </tr>
            <tr>
                <td><b>ยอดที่ทำการชำระแล้ว</b></td>
                <td><b>{{ $registration22->people * 200 }} บาท</b></td>
            </tr>
        </table>
    </p>

    <p>
        ​ขอขอบพระคุณที่เข้าร่วมเป็นส่วนหนึ่งของการแข่งขัน On The Way To the Champion Part 2<br>
        หากมีข้อมงสัย กรุณาติดต่อ<br>
        Facebook : <b>OTWesports</b><br>
        Line : <b>@otwjuniorleague</b> (มี@ด้วย)
        โทรศัพท์ : <b>020775056</b><br>
        ภายในเวลา 10:00 - 21:00 น.<br>
    </p>




@endsection
