@extends('emails.layout')

@section('title', 'ได้รับข้อมูลการแจ้งชำระเงินเรียบร้อยแล้ว')

@section('content')


    <img style="width: 100%" src="<?php echo $message->embed(resource_path('img/email/RoVPart2_cover_otc123132.jpg')); ?>">

    <div class="row breadcrumb my-4">
        <span style="margin: auto; font-weight: bold;">
            Reference Number:
            <span style="font-weight: normal;">
                {{ $registration2->ref }}
            </span>
        </span>
        <span style="margin: auto; font-weight: bold;">
            วันเวลาที่แจ้งชำระเงิน :
            <span style="font-weight: normal;">
                {{ $payment->created_at }}
            </span>
        </span>
    </div>

    <div class="row mb-2">
        <div class="col-md-2">
            <b>ชื่อโรงเรียน</b>
        </div>
        <div class="col-md-4">
            <span>{{ $registration2->school }}</span>
        </div>
        <div class="col-md-2">
            <b>ชื่อทีม</b>
        </div>
        <div class="col-md-4">
            <span>{{ $registration2->team }}</span>
        </div>
    </div>

    <p>
        <b>ยอดเงินที่โอน</b><br>
        {{ $payment->amount }} บาท<br><br>

        <b>วันที่-เวลาที่โอน</b><br>
        {{ $payment->datetime }}<br><br>

        <b>ธนาคารที่โอนเข้า</b><br><br>
        @if ($payment->to_bank == 'SCB')
        <p>
            <img src="<?php echo $message->embed(resource_path('img/email/scb.png')); ?>"> ธนาคารไทยพาณิชย์ สาขาโลตัส ทาวน์ อิน ทาวน์ ประเภทกระแสรายวัน
            <br>ชื่อบัญชี บริษัท บีเคเค ดีเวลอปเปอร์ จำกัด
            <br>เลขที่บัญชี 468-066918-9
        </p><br>
        @endif
        @if ($payment->to_bank == 'KBANK')
        <p>
            <img src="<?php echo $message->embed(resource_path('img/email/kbank.png')); ?>"> ธนาคารกสิกรไทย สาขาโลตัส ทาวน์ อิน ทาวน์ ประเภทกระแสรายวัน
            <br>ชื่อบัญชี บริษัท บีเคเค ดีเวลอปเปอร์ จำกัด
            <br>เลขที่บัญชี 032-3-33512-5
        </p><br>
        @endif

        <b>หมายเหตุ</b><br>
        {{ $payment->note }}<br>
    </p>

    <p>
        ​ขอขอบพระคุณที่เข้าร่วมเป็นส่วนหนึ่งของการแข่งขัน On The Way To the Champion Part 2<br>
        หากมีข้อมงสัย กรุณาติดต่อ<br>
        Facebook : <b>OTWesports</b><br>
        Line : <b>@otwjuniorleague</b> (มี@ด้วย)
        โทรศัพท์ : <b>020775056</b><br>
        ภายในเวลา 10:00 - 21:00 น.<br>
    </p>




@endsection
