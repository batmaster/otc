@extends('layouts.layout')

@section('title', 'OTW To The Champion Dota2 | Registration')

@section('style')
    <style>

    .nav-tabs {
        overflow-x: auto;
        overflow-y: hidden;
        display: -webkit-box;
        display: -moz-box;
    }

    .nav-tabs>li {
        float:none;
    }

    </style>
@endsection

@section('content')
    <div style="background-image: url('{{ asset('assets/images/Dot1.jpg')}}'); background-repeat: no-repeat; background-size: contain; background-color: #D6D0E1; padding-top: 10%; padding-bottom: 20px">

        <div class="container" style="background: white; padding: 3%; max-width: 800;position: relative; left: 0; top: 0; box-shadow: 0px -10px #C52431;">
            <h2>ON THE WAY TO THE CHAMPION DOTA2</h2>

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <p>เปิดโอกาสให้น้อง ๆ เยาวชนและประชาชนทั่วไปไม่จำกัดอายุ ทั่วประเทศ มาเข้าร่วมการแข่งขันเกม Dota 2 พร้อมชิงรางวัลมูลค่ารวมกว่า 500,000 บาท!!!
    ซึ่งเปิดรับสมัครถึง 512 ทีม !!! เริ่มเปิดรับสมัครแล้วตั้งแต่วันนี้ - 18 เมษายน 2561 เวลา 23.59 น.
            </p>

            <p><h5>*โปรดติดตามรายละเอียดการแข่งขันได้ที่เพจ</h5>
                Facebook : <b><a href="https://www.facebook.com/OTWDota2Esports">OTWDota2Esports</a></b><br>
                Line : <b>@otwjuniorleague</b> (มี@ด้วย)
            </p>

            <img src="{{ asset('assets/images/Poster_PART2_.png')}}" style="width: 100%; margin-bottom: 30px; display: none"></img>

            <hr>
            <ul class="nav nav-tabs">
                <li class="active"><a href="#team">ข้อมูลทีม</a></li>
                <li><a href="#member1">คนที่ 1</a></li>
                <li><a href="#member2">คนที่ 2</a></li>
                <li><a href="#member3">คนที่ 3</a></li>
                <li><a href="#member4">คนที่ 4</a></li>
                <li><a href="#member5">คนที่ 5</a></li>
                <li><a href="#member6">คนที่ 6</a></li>
                <li><a href="#member7">คนที่ 7</a></li>
                <li><a href="#member8">คนที่ 8</a></li>
            </ul>

            <form method="POST" action="{{ url('registered-d2') }}" id="form-registration" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="tab-content">
                    <div id="team" class="tab-pane fade in active">
                        <h3 style="margin-top: 50px; background-color: #9C222F; padding: 20; color: white">แบบลงทะเบียนการแข่งขันออนไลน์ (กรุณากรอกข้อมูลให้ครบถ้วน)</h3>
                        <div class="form-group">
                            <label for="input-team">ชื่อทีม</label>
                            <input type="text" class="form-control" id="input-team" name="input-team" value="{{ old('input-team') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="file-image-team">กรุณาถ่ายรูปทีม 1 รูป (พร้อมสมาชิก 5 คน)</label>
                            <input type="file" class="form-control-file" id="file-image-team" name="file-image-team" aria-describedby="fileHelp">
                        </div>
                        <div class="form-group">
                            <label for="file-image-team">โลโก้</label>
                            <input type="file" class="form-control-file" id="file-image-logo" name="file-image-logo" aria-describedby="fileHelp">
                        </div>
                        <h4><i>หมายเหตุ ผู้ที่สมัครผ่านโทรศัพท์มือถือ แนะนำให้กรอกข้อมูลโดยไม่ต้องใส่ภาพ แล้วค่อยส่งภาพมาทาง Line: @otwjuniorleague ได้คะ</i><h4>
                        <button type="button" class="btn btn-primary pull-right" style="padding: 15; margin-top: 20;" id="next0">ต่อไป</button>
                    </div>
                    <div id="member1" class="tab-pane fade">
                        <h3 style="margin-top: 50px; background-color: #9C222F; padding: 20; color: white">สมาชิกคนที่ 1 (หัวหน้าทีม)</h3>
                        <div class="form-group">
                            <label for="input-player1-name">ชื่อ - นามสกุล</label>
                            <input type="text" class="form-control" id="input-player1-name" name="input-player1-name" value="{{ old('input-player1-name') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player1-age">อายุ</label>
                            <input type="number" class="form-control" id="input-player1-age" name="input-player1-age" value="{{ old('input-player1-age') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player1-steam">STEAM ID <a href="how-to-get-steam" target="_blank">*</a></label>
                            <input type="text" class="form-control" id="input-player1-steam" name="input-player1-steam" value="{{ old('input-player1-steam') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player1-phone">เบอร์โทรศัพท์</label>
                            <input type="text" class="form-control" id="input-player1-phone" name="input-player1-phone" value="{{ old('input-player1-phone') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player1-line">LINE ID</label>
                            <input type="text" class="form-control" id="input-player1-line" name="input-player1-line" value="{{ old('input-player1-line') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player1-fb">Facebook</label>
                            <input type="text" class="form-control" id="input-player1-fb" name="input-player1-fb" value="{{ old('input-player1-fb') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player1-email">Email</label>
                            <input type="text" class="form-control" id="input-player1-email" name="input-player1-email" value="{{ old('input-player1-email') }}" required>
                        </div>

                        <div class="form-group">
                            <label for="input-player1-id">เลขบัตรประชาชน</label>
                            <input type="text" class="form-control" id="input-player1-id" name="input-player1-id" value="{{ old('input-player1-id') }}" required>
                        </div>
                        <button type="button" class="btn btn-primary" style="padding: 15; margin-top: 20;" id="previous1">กลับ</button>
                        <button type="button" class="btn btn-primary pull-right" style="padding: 15; margin-top: 20;" id="next1">ต่อไป</button>
                    </div>
                    <div id="member2" class="tab-pane fade">
                        <h3 style="margin-top: 50px; background-color: #9C222F; padding: 20; color: white">สมาชิกคนที่ 2</h3>
                        <div class="form-group">
                            <label for="input-player2-name">ชื่อ - นามสกุล</label>
                            <input type="text" class="form-control" id="input-player2-name" name="input-player2-name" value="{{ old('input-player2-name') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player2-age">อายุ</label>
                            <input type="number" class="form-control" id="input-player2-age" name="input-player2-age" value="{{ old('input-player2-age') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player2-steam">STEAM ID</label>
                            <input type="text" class="form-control" id="input-player2-steam" name="input-player2-steam" value="{{ old('input-player2-steam') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player1-id">เลขบัตรประชาชน</label>
                            <input type="text" class="form-control" id="input-player2-id" name="input-player2-id" value="{{ old('input-player2-id') }}" required>
                        </div>
                        <button type="button" class="btn btn-primary" style="padding: 15; margin-top: 20;" id="previous2">กลับ</button>
                        <button type="button" class="btn btn-primary pull-right" style="padding: 15; margin-top: 20;" id="next2">ต่อไป</button>
                    </div>
                    <div id="member3" class="tab-pane fade">
                        <h3 style="margin-top: 50px; background-color: #9C222F; padding: 20; color: white">สมาชิกคนที่ 3</h3>
                        <div class="form-group">
                            <label for="input-player3-name">ชื่อ - นามสกุล</label>
                            <input type="text" class="form-control" id="input-player3-name" name="input-player3-name" value="{{ old('input-player3-name') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player3-age">อายุ</label>
                            <input type="number" class="form-control" id="input-player3-age" name="input-player3-age" value="{{ old('input-player3-age') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player3-steam">STEAM ID</label>
                            <input type="text" class="form-control" id="input-player3-steam" name="input-player3-steam" value="{{ old('input-player3-steam') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player3-id">เลขบัตรประชาชน</label>
                            <input type="text" class="form-control" id="input-player3-id" name="input-player3-id" value="{{ old('input-player3-id') }}" required>
                        </div>
                        <button type="button" class="btn btn-primary" style="padding: 15; margin-top: 20;" id="previous3">กลับ</button>
                        <button type="button" class="btn btn-primary pull-right" style="padding: 15; margin-top: 20;" id="next3">ต่อไป</button>
                    </div>
                    <div id="member4" class="tab-pane fade">
                        <h3 style="margin-top: 50px; background-color: #9C222F; padding: 20; color: white">สมาชิกคนที่ 4</h3>
                        <div class="form-group">
                            <label for="input-player4-name">ชื่อ - นามสกุล</label>
                            <input type="text" class="form-control" id="input-player4-name" name="input-player4-name" value="{{ old('input-player4-name') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player4-age">อายุ</label>
                            <input type="number" class="form-control" id="input-player4-age" name="input-player4-age" value="{{ old('input-player4-age') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player4-steam">STEAM ID</label>
                            <input type="text" class="form-control" id="input-player4-steam" name="input-player4-steam" value="{{ old('input-player4-steam') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player4-id">เลขบัตรประชาชน</label>
                            <input type="text" class="form-control" id="input-player4-id" name="input-player4-id" value="{{ old('input-player4-id') }}" required>
                        </div>
                        <button type="button" class="btn btn-primary" style="padding: 15; margin-top: 20;" id="previous4">กลับ</button>
                        <button type="button" class="btn btn-primary pull-right" style="padding: 15; margin-top: 20;" id="next4">ต่อไป</button>
                    </div>
                    <div id="member5" class="tab-pane fade">
                        <h3 style="margin-top: 50px; background-color: #9C222F; padding: 20; color: white">สมาชิกคนที่ 5</h3>
                        <div class="form-group">
                            <label for="input-player5-name">ชื่อ - นามสกุล</label>
                            <input type="text" class="form-control" id="input-player5-name" name="input-player5-name" value="{{ old('input-player5-name') }}"required>
                        </div>
                        <div class="form-group">
                            <label for="input-player5-age">อายุ</label>
                            <input type="number" class="form-control" id="input-player5-age" name="input-player5-age" value="{{ old('input-player5-age') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player5-steam">STEAM ID</label>
                            <input type="text" class="form-control" id="input-player5-steam" name="input-player5-steam" value="{{ old('input-player5-steam') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player5-id">เลขบัตรประชาชน</label>
                            <input type="text" class="form-control" id="input-player5-id" name="input-player5-id" value="{{ old('input-player5-id') }}" required>
                        </div>
                        <button type="button" class="btn btn-primary" style="padding: 15; margin-top: 20;" id="previous5">กลับ</button>
                        <button type="button" class="btn btn-primary pull-right" style="padding: 15; margin-top: 20;" id="next5">ต่อไป</button>
                    </div>
                    <div id="member6" class="tab-pane fade">
                        <h3 style="margin-top: 50px; background-color: #9C222F; padding: 20; color: white">สมาชิกสำรองคนที่ 1</h3>
                        <i>โปรดระบุ ถ้ามี</i>
                        <div class="form-group">
                            <label for="input-player6-name">ชื่อ - นามสกุล</label>
                            <input type="text" class="form-control" id="input-player6-name" name="input-player6-name" value="{{ old('input-player6-name') }}">
                        </div>
                        <div class="form-group">
                            <label for="input-player6-age">อายุ</label>
                            <input type="number" class="form-control" id="input-player6-age" name="input-player6-age" value="{{ old('input-player6-age') }}">
                        </div>
                        <div class="form-group">
                            <label for="input-player6-steam">STEAM ID</label>
                            <input type="text" class="form-control" id="input-player6-steam" name="input-player6-steam" value="{{ old('input-player6-steam') }}">
                        </div>
                        <div class="form-group">
                            <label for="input-player6-id">เลขบัตรประชาชน</label>
                            <input type="text" class="form-control" id="input-player6-id" name="input-player6-id" value="{{ old('input-player6-id') }}">
                        </div>
                        <button type="button" class="btn btn-primary" style="padding: 15; margin-top: 20;" id="previous6">กลับ</button>
                        <button type="button" class="btn btn-primary pull-right" style="padding: 15; margin-top: 20;" id="next6">ต่อไป</button>
                    </div>
                    <div id="member7" class="tab-pane fade">
                        <h3 style="margin-top: 50px; background-color: #9C222F; padding: 20; color: white">สมาชิกสำรองคนที่ 2</h3>
                        <i>โปรดระบุ ถ้ามี</i>
                        <div class="form-group">
                            <label for="input-player7-name">ชื่อ - นามสกุล</label>
                            <input type="text" class="form-control" id="input-player7-name" name="input-player7-name" value="{{ old('input-player7-name') }}">
                        </div>
                        <div class="form-group">
                            <label for="input-player7-age">อายุ</label>
                            <input type="number" class="form-control" id="input-player7-age" name="input-player7-age" value="{{ old('input-player7-age') }}">
                        </div>
                        <div class="form-group">
                            <label for="input-player7-steam">STEAM ID</label>
                            <input type="text" class="form-control" id="input-player7-steam" name="input-player7-steam" value="{{ old('input-player7-steam') }}">
                        </div>
                        <div class="form-group">
                            <label for="input-player7-id">เลขบัตรประชาชน</label>
                            <input type="text" class="form-control" id="input-player7-id" name="input-player7-id" value="{{ old('input-player7-id') }}">
                        </div>
                        <button type="button" class="btn btn-primary" style="padding: 15; margin-top: 20;" id="previous7">กลับ</button>
                        <button type="button" class="btn btn-primary pull-right" style="padding: 15; margin-top: 20;" id="next7">ต่อไป</button>
                    </div>
                    <div id="member8" class="tab-pane fade">
                        <h3 style="margin-top: 50px; background-color: #9C222F; padding: 20; color: white">สมาชิกสำรองคนที่ 3</h3>
                        <i>โปรดระบุ ถ้ามี</i>
                        <div class="form-group">
                            <label for="input-player8-name">ชื่อ - นามสกุล</label>
                            <input type="text" class="form-control" id="input-player8-name" name="input-player8-name" value="{{ old('input-player8-name') }}">
                        </div>
                        <div class="form-group">
                            <label for="input-player8-age">อายุ</label>
                            <input type="number" class="form-control" id="input-player8-age" name="input-player8-age" value="{{ old('input-player8-age') }}">
                        </div>
                        <div class="form-group">
                            <label for="input-player8-steam">STEAM ID</label>
                            <input type="text" class="form-control" id="input-player8-steam" name="input-player8-steam" value="{{ old('input-player8-steam') }}">
                        </div>
                        <div class="form-group">
                            <label for="input-player8-id">เลขบัตรประชาชน</label>
                            <input type="text" class="form-control" id="input-player8-id" name="input-player8-id" value="{{ old('input-player8-id') }}">
                        </div>
                        <button type="button" class="btn btn-primary" style="padding: 15; margin-top: 20;" id="previous8">กลับ</button>
                        <button type="submit" class="btn btn-success pull-right" style="padding: 15; margin-top: 20;">สมัคร</button>
                    </div>

                </div>
            </form>
        </div>
    </div>

    <script>
    $(document).ready(function(){
        $(".nav-tabs a").click(function(){
            $(this).tab('show');
        });
    });

    $("#next0").click(function() {
        $('.nav-tabs a[href="#member1"]').trigger('click');
    });
    $("#next1").click(function() {
        $('.nav-tabs a[href="#member2"]').trigger('click');
    });
    $("#next2").click(function() {
        $('.nav-tabs a[href="#member3"]').trigger('click');
    });
    $("#next3").click(function() {
        $('.nav-tabs a[href="#member4"]').trigger('click');
    });
    $("#next4").click(function() {
        $('.nav-tabs a[href="#member5"]').trigger('click');
    });
    $("#next5").click(function() {
        $('.nav-tabs a[href="#member6"]').trigger('click');
    });
    $("#next6").click(function() {
        $('.nav-tabs a[href="#member7"]').trigger('click');
    });
    $("#next7").click(function() {
        $('.nav-tabs a[href="#member8"]').trigger('click');
    });

    $("#previous1").click(function() {
        $('.nav-tabs a[href="#member0"]').trigger('click');
    });
    $("#previous2").click(function() {
        $('.nav-tabs a[href="#member1"]').trigger('click');
    });
    $("#previous3").click(function() {
        $('.nav-tabs a[href="#member2"]').trigger('click');
    });
    $("#previous4").click(function() {
        $('.nav-tabs a[href="#member3"]').trigger('click');
    });
    $("#previous5").click(function() {
        $('.nav-tabs a[href="#member4"]').trigger('click');
    });
    $("#previous6").click(function() {
        $('.nav-tabs a[href="#member5"]').trigger('click');
    });
    $("#previous7").click(function() {
        $('.nav-tabs a[href="#member6"]').trigger('click');
    });

    $('#form-registration').validate({
        ignore: ".ignore",
        invalidHandler: function(e, validator){
            if(validator.errorList.length)
            $('.nav-tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
        }
    });
</script>
@endsection
