@extends('layouts.layout')

@section('title', 'OTW To The Champion | Rule')

@section('style')
    <style>

    </style>
@endsection


@section('content')
<div class="container" style="padding: 5%">
            <h3>วิธีการรับ STEAM ID</h3>
            <p>1. เข้าไปที่หน้า steam กดที่ชื่อของเรา และเลือกคำว่า profile</p>
            <center>
                <img class="thumbnail" src="{{ asset('assets/images/howtogetsteam1.png')}}">
            </center>

            <p>2. คลิกขวาที่ชื่อของเรา แล้วเลือก Copy Page URL</p>
            <center>
                <img class="thumbnail" src="{{ asset('assets/images/howtogetsteam2.png')}}">
            </center>

            <p>3. เข้าไปที่เว็บไซต์ http://steamidfinder.com แล้วนำ url ที่เราได้มา วางลงไปแล้วกดค้นหา</p>
            <center>
                <img class="thumbnail" src="{{ asset('assets/images/howtogetsteam3.png')}}">
            </center>

            <p>4. แค่นี้ก็จะได้ STEAM ID ของเราแล้ว ดังตัวอย่าง คือ STEAM_1:1:68670160</p>
            <center>
                <img class="thumbnail" src="{{ asset('assets/images/howtogetsteam4.png')}}">
            </center>
</div>

@endsection
