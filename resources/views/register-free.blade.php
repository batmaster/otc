@extends('layouts.layout')

@section('title', 'OTW To The Champion | Free Registration')

@section('style')
    <style>

    .nav-tabs {
        overflow-x: auto;
        overflow-y: hidden;
        display: -webkit-box;
        display: -moz-box;
    }

    .nav-tabs>li {
        float:none;
    }

    </style>
@endsection

@section('content')
    <div style="background-image: url('{{ asset('assets/images/RoVHome_01.jpg')}}'); background-repeat: no-repeat; background-size: contain; background-color: #D6D0E1; padding-top: 10%; padding-bottom: 20px">

        <div class="container" style="background: white; padding: 3%; max-width: 800;position: relative; left: 0; top: 0; box-shadow: 0px -10px #C52431;">
            <h2>ON THE WAY TO THE CHAMPION</h2>

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <p>
                เปิดโอกาสให้น้องๆ ระดับมัธยมศึกษาหรือเทียบเท่าที่มีอายุไม่เกิน 18 ปี และบุคคลทั่วไปไม่จำกัดอายุ มาเข้าร่วมการแข่งขันเกม Rov เพื่อค้นหาแชมป์เพียงหนึ่งเดียวในเขตพื้นที่กรุงเทพมหานครและปริมณฑล พร้อมชิงรางวัลมูลค่ารวมกว่า 1,000,000 บาท!!! และได้เซ็นต์สัญญาเป็นนักกีฬา eSports อย่างเต็มตัว ภายใต้บริษัท บีเคเค ดีเวลลอปเปอร์ จำกัด 1 ปี พร้อมเงินเดือน 20,000 บาท ต่อคน!!!
            </p>

            <p><h5>*โปรดติดตามรายละเอียดการแข่งขันได้ที่เพจ</h5>
                Facebook : <b>OTWesports</b><br>
                Line : <b>@otwjuniorleague</b> (มี@ด้วย)
            </p>

            <img src="{{ asset('assets/images/Poster_PART2_.png')}}" style="width: 100%; margin-bottom: 30px"></img>

            <hr>
            <ul class="nav nav-tabs">
                <li class="active"><a href="#team">ข้อมูลทีม</a></li>
                <li><a href="#member1">คนที่ 1</a></li>
                <li><a href="#member2">คนที่ 2</a></li>
                <li><a href="#member3">คนที่ 3</a></li>
                <li><a href="#member4">คนที่ 4</a></li>
                <li><a href="#member5">คนที่ 5</a></li>
                <li><a href="#member6">คนที่ 6</a></li>
                <li><a href="#member7">คนที่ 7</a></li>
                <li><a href="#member8">คนที่ 8</a></li>
            </ul>

            <form method="POST" action="{{ url('registered-free') }}" id="form-registration" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="tab-content">
                    <div id="team" class="tab-pane fade in active">
                        <h3 style="margin-top: 50px; background-color: #9C222F; padding: 20; color: white">แบบลงทะเบียนการแข่งขันออนไลน์ (กรุณากรอกข้อมูลให้ครบถ้วน)</h3>
                        <div class="form-group">
                            <label for="input-school">ชื่อโรงเรียน</label>
                            <input type="text" class="form-control" id="input-school" name="input-school" value="{{ old('input-school') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-team">ชื่อทีม</label>
                            <input type="text" class="form-control" id="input-team" name="input-team" value="{{ old('input-team') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="file-image-team">กรุณาถ่ายรูปทีม 1 รูป (พร้อมสมาชิก 5 คน)</label>
                            <input type="file" class="form-control-file" id="file-image-team" name="file-image-team" aria-describedby="fileHelp">
                        </div>
                        <div class="form-group">
                            <label for="file-image-team">โลโก้</label>
                            <input type="file" class="form-control-file" id="file-image-logo" name="file-image-logo" aria-describedby="fileHelp">
                        </div>
                        <h4><i>หมายเหตุ ผู้ที่สมัครผ่านโทรศัพท์มือถือ แนะนำให้กรอกข้อมูลโดยไม่ต้องใส่ภาพ แล้วค่อยส่งภาพมาทาง Line: @otwjuniorleague ได้คะ</i><h4>
                        <button type="button" class="btn btn-primary pull-right" style="padding: 15; margin-top: 20;" id="next0">ต่อไป</button>
                    </div>
                    <div id="member1" class="tab-pane fade">
                        <h3 style="margin-top: 50px; background-color: #9C222F; padding: 20; color: white">สมาชิกคนที่ 1 (หัวหน้าทีม)</h3>
                        <div class="form-group">
                            <label for="input-player1-name">ชื่อ - นามสกุล</label>
                            <input type="text" class="form-control" id="input-player1-name" name="input-player1-name" value="{{ old('input-player1-name') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player1-phone">เบอร์โทรศัพท์</label>
                            <input type="text" class="form-control" id="input-player1-phone" name="input-player1-phone" value="{{ old('input-player1-phone') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player1-line">LINE ID</label>
                            <input type="text" class="form-control" id="input-player1-line" name="input-player1-line" value="{{ old('input-player1-line') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player1-fb">Facebook</label>
                            <input type="text" class="form-control" id="input-player1-fb" name="input-player1-fb" value="{{ old('input-player1-fb') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player1-email">Email</label>
                            <input type="text" class="form-control" id="input-player1-email" name="input-player1-email" value="{{ old('input-player1-email') }}" required>
                        </div>
                        <fieldset class="form-group">
                            <label>เพศ</label>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player1-gender" value="male" {{ (empty(old('radio-player1-gender')) || old('radio-player1-gender') == 'male') ? 'checked' : '' }}>
                                    ชาย
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player1-gender" value="female" {{ (old('radio-player1-gender') == 'female') ? 'checked' : '' }}>
                                    หญิง
                                </label>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <label>ระดับชั้น</label>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player1-grade" value="1" {{ (empty(old('radio-player1-grade')) || old('radio-player1-grade') == '1') ? 'checked' : '' }}>
                                    ม.1
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player1-grade" value="2" {{ (old('radio-player1-grade') == '2') ? 'checked' : '' }}>
                                    ม.2
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player1-grade" value="3" {{ (old('radio-player1-grade') == '3') ? 'checked' : '' }}>
                                    ม.3
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player1-grade" value="4" {{ (old('radio-player1-grade') == '4') ? 'checked' : '' }}>
                                    ม.4
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player1-grade" value="5" {{ (old('radio-player1-grade') == '5') ? 'checked' : '' }}>
                                    ม.5
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player1-grade" value="6" {{ (old('radio-player1-grade') == '6') ? 'checked' : '' }}>
                                    ม.6
                                </label>
                            </div>
                        </fieldset>
                        <div class="form-group">
                            <label for="input-player1-id">รหัสประจำตัวนักเรียน</label>
                            <input type="text" class="form-control" id="input-player1-id" name="input-player1-id" value="{{ old('input-player1-id') }}" required>
                        </div>
                        <button type="button" class="btn btn-primary" style="padding: 15; margin-top: 20;" id="previous1">กลับ</button>
                        <button type="button" class="btn btn-primary pull-right" style="padding: 15; margin-top: 20;" id="next1">ต่อไป</button>
                    </div>
                    <div id="member2" class="tab-pane fade">
                        <h3 style="margin-top: 50px; background-color: #9C222F; padding: 20; color: white">สมาชิกคนที่ 2</h3>
                        <div class="form-group">
                            <label for="input-player2-name">ชื่อ - นามสกุล</label>
                            <input type="text" class="form-control" id="input-player2-name" name="input-player2-name" value="{{ old('input-player2-name') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player2-phone">เบอร์โทรศัพท์</label>
                            <input type="text" class="form-control" id="input-player2-phone" name="input-player2-phone" value="{{ old('input-player2-phone') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player2-fb">Facebook</label>
                            <input type="text" class="form-control" id="input-player2-fb" name="input-player2-fb" value="{{ old('input-player2-fb') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player2-email">Email</label>
                            <input type="text" class="form-control" id="input-player2-email" name="input-player2-email" value="{{ old('input-player2-email') }}" required>
                        </div>
                        <fieldset class="form-group">
                            <label>เพศ</label>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player2-gender" value="male" {{ (empty(old('radio-player2-gender')) || old('radio-player2-gender') == 'male') ? 'checked' : '' }}>
                                    ชาย
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player2-gender" value="female" {{ (old('radio-player2-gender') == 'female') ? 'checked' : '' }}>
                                    หญิง
                                </label>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <label>ระดับชั้น</label>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player2-grade" value="1" {{ (empty(old('radio-player2-grade')) || old('radio-player2-grade') == '1') ? 'checked' : '' }}>
                                    ม.1
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player2-grade" value="2" {{ (old('radio-player2-grade') == '2') ? 'checked' : '' }}>
                                    ม.2
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player2-grade" value="3" {{ (old('radio-player2-grade') == '3') ? 'checked' : '' }}>
                                    ม.3
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player2-grade" value="4" {{ (old('radio-player2-grade') == '4') ? 'checked' : '' }}>
                                    ม.4
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player2-grade" value="5" {{ (old('radio-player2-grade') == '5') ? 'checked' : '' }}>
                                    ม.5
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player2-grade" value="6" {{ (old('radio-player2-grade') == '6') ? 'checked' : '' }}>
                                    ม.6
                                </label>
                            </div>
                        </fieldset>
                        <div class="form-group">
                            <label for="input-player1-id">รหัสประจำตัวนักเรียน</label>
                            <input type="text" class="form-control" id="input-player2-id" name="input-player2-id" value="{{ old('input-player2-id') }}" required>
                        </div>
                        <button type="button" class="btn btn-primary" style="padding: 15; margin-top: 20;" id="previous2">กลับ</button>
                        <button type="button" class="btn btn-primary pull-right" style="padding: 15; margin-top: 20;" id="next2">ต่อไป</button>
                    </div>
                    <div id="member3" class="tab-pane fade">
                        <h3 style="margin-top: 50px; background-color: #9C222F; padding: 20; color: white">สมาชิกคนที่ 3</h3>
                        <div class="form-group">
                            <label for="input-player3-name">ชื่อ - นามสกุล</label>
                            <input type="text" class="form-control" id="input-player3-name" name="input-player3-name" value="{{ old('input-player3-name') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player3-phone">เบอร์โทรศัพท์</label>
                            <input type="text" class="form-control" id="input-player3-phone" name="input-player3-phone" value="{{ old('input-player3-phone') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player3-fb">Facebook</label>
                            <input type="text" class="form-control" id="input-player3-fb" name="input-player3-fb" value="{{ old('input-player3-fb') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player3-email">Email</label>
                            <input type="text" class="form-control" id="input-player3-email" name="input-player3-email" value="{{ old('input-player3-email') }}" required>
                        </div>
                        <fieldset class="form-group">
                            <label>เพศ</label>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player3-gender" value="male" {{ (empty(old('radio-player3-gender')) || old('radio-player3-gender') == 'male') ? 'checked' : '' }}>
                                    ชาย
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player3-gender" value="female" {{ (old('radio-player3-gender') == 'female') ? 'checked' : '' }}>
                                    หญิง
                                </label>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <label>ระดับชั้น</label>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player3-grade" value="1" {{ (empty(old('radio-player3-grade')) || old('radio-player3-grade') == '1') ? 'checked' : '' }}>
                                    ม.1
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player3-grade" value="2" {{ (old('radio-player3-grade') == '2') ? 'checked' : '' }}>
                                    ม.2
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player3-grade" value="3" {{ (old('radio-player3-grade') == '3') ? 'checked' : '' }}>
                                    ม.3
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player3-grade" value="4" {{ (old('radio-player3-grade') == '4') ? 'checked' : '' }}>
                                    ม.4
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player3-grade" value="5" {{ (old('radio-player3-grade') == '5') ? 'checked' : '' }}>
                                    ม.5
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player3-grade" value="6" {{ (old('radio-player3-grade') == '6') ? 'checked' : '' }}>
                                    ม.6
                                </label>
                            </div>
                        </fieldset>
                        <div class="form-group">
                            <label for="input-player3-id">รหัสประจำตัวนักเรียน</label>
                            <input type="text" class="form-control" id="input-player3-id" name="input-player3-id" value="{{ old('input-player3-id') }}" required>
                        </div>
                        <button type="button" class="btn btn-primary" style="padding: 15; margin-top: 20;" id="previous3">กลับ</button>
                        <button type="button" class="btn btn-primary pull-right" style="padding: 15; margin-top: 20;" id="next3">ต่อไป</button>
                    </div>
                    <div id="member4" class="tab-pane fade">
                        <h3 style="margin-top: 50px; background-color: #9C222F; padding: 20; color: white">สมาชิกคนที่ 4</h3>
                        <div class="form-group">
                            <label for="input-player4-name">ชื่อ - นามสกุล</label>
                            <input type="text" class="form-control" id="input-player4-name" name="input-player4-name" value="{{ old('input-player4-name') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player4-phone">เบอร์โทรศัพท์</label>
                            <input type="text" class="form-control" id="input-player4-phone" name="input-player4-phone" value="{{ old('input-player4-phone') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player4-fb">Facebook</label>
                            <input type="text" class="form-control" id="input-player4-fb" name="input-player4-fb" value="{{ old('input-player4-fb') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player4-email">Email</label>
                            <input type="text" class="form-control" id="input-player4-email" name="input-player4-email" value="{{ old('input-player4-email') }}" required>
                        </div>
                        <fieldset class="form-group">
                            <label>เพศ</label>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player4-gender" value="male" {{ (empty(old('radio-player4-gender')) || old('radio-player4-gender') == 'male') ? 'checked' : '' }}>
                                    ชาย
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player4-gender" value="female" {{ (old('radio-player4-gender') == 'female') ? 'checked' : '' }}>
                                    หญิง
                                </label>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <label>ระดับชั้น</label>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player4-grade" value="1" {{ (empty(old('radio-player4-grade')) || old('radio-player4-grade') == '1') ? 'checked' : '' }}>
                                    ม.1
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player4-grade" value="2" {{ (old('radio-player4-grade') == '2') ? 'checked' : '' }}>
                                    ม.2
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player4-grade" value="3" {{ (old('radio-player4-grade') == '3') ? 'checked' : '' }}>
                                    ม.3
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player4-grade" value="4" {{ (old('radio-player4-grade') == '4') ? 'checked' : '' }}>
                                    ม.4
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player4-grade" value="5" {{ (old('radio-player4-grade') == '5') ? 'checked' : '' }}>
                                    ม.5
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player4-grade" value="6" {{ (old('radio-player4-grade') == '6') ? 'checked' : '' }}>
                                    ม.6
                                </label>
                            </div>
                        </fieldset>
                        <div class="form-group">
                            <label for="input-player4-id">รหัสประจำตัวนักเรียน</label>
                            <input type="text" class="form-control" id="input-player4-id" name="input-player4-id" value="{{ old('input-player4-id') }}" required>
                        </div>
                        <button type="button" class="btn btn-primary" style="padding: 15; margin-top: 20;" id="previous4">กลับ</button>
                        <button type="button" class="btn btn-primary pull-right" style="padding: 15; margin-top: 20;" id="next4">ต่อไป</button>
                    </div>
                    <div id="member5" class="tab-pane fade">
                        <h3 style="margin-top: 50px; background-color: #9C222F; padding: 20; color: white">สมาชิกคนที่ 5</h3>
                        <div class="form-group">
                            <label for="input-player5-name">ชื่อ - นามสกุล</label>
                            <input type="text" class="form-control" id="input-player5-name" name="input-player5-name" value="{{ old('input-player5-name') }}"required>
                        </div>
                        <div class="form-group">
                            <label for="input-player5-phone">เบอร์โทรศัพท์</label>
                            <input type="text" class="form-control" id="input-player5-phone" name="input-player5-phone" value="{{ old('input-player5-phone') }}"required>
                        </div>
                        <div class="form-group">
                            <label for="input-player5-fb">Facebook</label>
                            <input type="text" class="form-control" id="input-player5-fb" name="input-player5-fb" value="{{ old('input-player5-fb') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="input-player5-email">Email</label>
                            <input type="text" class="form-control" id="input-player5-email" name="input-player5-email" value="{{ old('input-player5-email') }}" required>
                        </div>
                        <fieldset class="form-group">
                            <label>เพศ</label>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player5-gender" value="male" {{ (empty(old('radio-player5-gender')) || old('radio-player5-gender') == 'male') ? 'checked' : '' }}>
                                    ชาย
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player5-gender" value="female" {{ (old('radio-player5-gender') == 'female') ? 'checked' : '' }}>
                                    หญิง
                                </label>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <label>ระดับชั้น</label>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player5-grade" value="1" {{ (empty(old('radio-player5-grade')) || old('radio-player5-grade') == '1') ? 'checked' : '' }}>
                                    ม.1
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player5-grade" value="2" {{ (old('radio-player5-grade') == '2') ? 'checked' : '' }}>
                                    ม.2
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player5-grade" value="3" {{ (old('radio-player5-grade') == '3') ? 'checked' : '' }}>
                                    ม.3
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player5-grade" value="4" {{ (old('radio-player5-grade') == '4') ? 'checked' : '' }}>
                                    ม.4
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player5-grade" value="5" {{ (old('radio-player5-grade') == '5') ? 'checked' : '' }}>
                                    ม.5
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player5-grade" value="6" {{ (old('radio-player5-grade') == '6') ? 'checked' : '' }}>
                                    ม.6
                                </label>
                            </div>
                        </fieldset>
                        <div class="form-group">
                            <label for="input-player5-id">รหัสประจำตัวนักเรียน</label>
                            <input type="text" class="form-control" id="input-player5-id" name="input-player5-id" value="{{ old('input-player5-id') }}" required>
                        </div>
                        <button type="button" class="btn btn-primary" style="padding: 15; margin-top: 20;" id="previous5">กลับ</button>
                        <button type="button" class="btn btn-primary pull-right" style="padding: 15; margin-top: 20;" id="next5">ต่อไป</button>
                    </div>
                    <div id="member6" class="tab-pane fade">
                        <h3 style="margin-top: 50px; background-color: #9C222F; padding: 20; color: white">สมาชิกสำรองคนที่ 1</h3>
                        <i>โปรดระบุ ถ้ามี</i>
                        <div class="form-group">
                            <label for="input-player6-name">ชื่อ - นามสกุล</label>
                            <input type="text" class="form-control" id="input-player6-name" name="input-player6-name" value="{{ old('input-player6-name') }}">
                        </div>
                        <div class="form-group">
                            <label for="input-player6-phone">เบอร์โทรศัพท์</label>
                            <input type="text" class="form-control" id="input-player6-phone" name="input-player6-phone" value="{{ old('input-player6-phone') }}">
                        </div>
                        <div class="form-group">
                            <label for="input-player6-fb">Facebook</label>
                            <input type="text" class="form-control" id="input-player6-fb" name="input-player6-fb" value="{{ old('input-player6-fb') }}">
                        </div>
                        <div class="form-group">
                            <label for="input-player6-email">Email</label>
                            <input type="text" class="form-control" id="input-player6-email" name="input-player6-email" value="{{ old('input-player6-email') }}">
                        </div>
                        <fieldset class="form-group">
                            <label>เพศ</label>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player6-gender" value="male" {{ (empty(old('radio-player6-gender')) || old('radio-player6-gender') == 'male') ? 'checked' : '' }}>
                                    ชาย
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player6-gender" value="female" {{ (old('radio-player6-gender') == 'female') ? 'checked' : '' }}>
                                    หญิง
                                </label>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <label>ระดับชั้น</label>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player6-grade" value="1" {{ (empty(old('radio-player6-grade')) || old('radio-player6-grade') == '1') ? 'checked' : '' }}>
                                    ม.1
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player6-grade" value="2" {{ (old('radio-player6-grade') == '2') ? 'checked' : '' }}>
                                    ม.2
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player6-grade" value="3" {{ (old('radio-player6-grade') == '3') ? 'checked' : '' }}>
                                    ม.3
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player6-grade" value="4" {{ (old('radio-player6-grade') == '4') ? 'checked' : '' }}>
                                    ม.4
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player6-grade" value="5" {{ (old('radio-player6-grade') == '5') ? 'checked' : '' }}>
                                    ม.5
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player6-grade" value="6" {{ (old('radio-player6-grade') == '6') ? 'checked' : '' }}>
                                    ม.6
                                </label>
                            </div>
                        </fieldset>
                        <div class="form-group">
                            <label for="input-player6-id">รหัสประจำตัวนักเรียน</label>
                            <input type="text" class="form-control" id="input-player6-id" name="input-player6-id" value="{{ old('input-player6-id') }}">
                        </div>
                        <button type="button" class="btn btn-primary" style="padding: 15; margin-top: 20;" id="previous6">กลับ</button>
                        <button type="button" class="btn btn-primary pull-right" style="padding: 15; margin-top: 20;" id="next6">ต่อไป</button>
                    </div>
                    <div id="member7" class="tab-pane fade">
                        <h3 style="margin-top: 50px; background-color: #9C222F; padding: 20; color: white">สมาชิกสำรองคนที่ 2</h3>
                        <i>โปรดระบุ ถ้ามี</i>
                        <div class="form-group">
                            <label for="input-player7-name">ชื่อ - นามสกุล</label>
                            <input type="text" class="form-control" id="input-player7-name" name="input-player7-name" value="{{ old('input-player7-name') }}">
                        </div>
                        <div class="form-group">
                            <label for="input-player7-phone">เบอร์โทรศัพท์</label>
                            <input type="text" class="form-control" id="input-player7-phone" name="input-player7-phone" value="{{ old('input-player7-phone') }}">
                        </div>
                        <div class="form-group">
                            <label for="input-player7-fb">Facebook</label>
                            <input type="text" class="form-control" id="input-player7-fb" name="input-player7-fb" value="{{ old('input-player7-fb') }}">
                        </div>
                        <div class="form-group">
                            <label for="input-player7-email">Email</label>
                            <input type="text" class="form-control" id="input-player7-email" name="input-player7-email" value="{{ old('input-player7-email') }}">
                        </div>
                        <fieldset class="form-group">
                            <label>เพศ</label>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player7-gender" value="male" {{ (empty(old('radio-player7-gender')) || old('radio-player7-gender') == 'male') ? 'checked' : '' }}>
                                    ชาย
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player7-gender" value="female" {{ (old('radio-player7-gender') == 'female') ? 'checked' : '' }}>
                                    หญิง
                                </label>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <label>ระดับชั้น</label>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player7-grade" value="1" {{ (empty(old('radio-player7-grade')) || old('radio-player6-grade') == '1') ? 'checked' : '' }}>
                                    ม.1
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player7-grade" value="2" {{ (old('radio-player7-grade') == '2') ? 'checked' : '' }}>
                                    ม.2
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player7-grade" value="3" {{ (old('radio-player7-grade') == '3') ? 'checked' : '' }}>
                                    ม.3
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player7-grade" value="4" {{ (old('radio-player7-grade') == '4') ? 'checked' : '' }}>
                                    ม.4
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player7-grade" value="5" {{ (old('radio-player7-grade') == '5') ? 'checked' : '' }}>
                                    ม.5
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player7-grade" value="6" {{ (old('radio-player7-grade') == '6') ? 'checked' : '' }}>
                                    ม.6
                                </label>
                            </div>
                        </fieldset>
                        <div class="form-group">
                            <label for="input-player7-id">รหัสประจำตัวนักเรียน</label>
                            <input type="text" class="form-control" id="input-player7-id" name="input-player7-id" value="{{ old('input-player7-id') }}">
                        </div>
                        <button type="button" class="btn btn-primary" style="padding: 15; margin-top: 20;" id="previous7">กลับ</button>
                        <button type="button" class="btn btn-primary pull-right" style="padding: 15; margin-top: 20;" id="next7">ต่อไป</button>
                    </div>
                    <div id="member8" class="tab-pane fade">
                        <h3 style="margin-top: 50px; background-color: #9C222F; padding: 20; color: white">สมาชิกสำรองคนที่ 3</h3>
                        <i>โปรดระบุ ถ้ามี</i>
                        <div class="form-group">
                            <label for="input-player8-name">ชื่อ - นามสกุล</label>
                            <input type="text" class="form-control" id="input-player8-name" name="input-player8-name" value="{{ old('input-player8-name') }}">
                        </div>
                        <div class="form-group">
                            <label for="input-player8-phone">เบอร์โทรศัพท์</label>
                            <input type="text" class="form-control" id="input-player8-phone" name="input-player8-phone" value="{{ old('input-player8-phone') }}">
                        </div>
                        <div class="form-group">
                            <label for="input-player8-fb">Facebook</label>
                            <input type="text" class="form-control" id="input-player8-fb" name="input-player8-fb" value="{{ old('input-player8-fb') }}">
                        </div>
                        <div class="form-group">
                            <label for="input-player8-email">Email</label>
                            <input type="text" class="form-control" id="input-player8-email" name="input-player8-email" value="{{ old('input-player8-email') }}">
                        </div>
                        <fieldset class="form-group">
                            <label>เพศ</label>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player8-gender" value="male" {{ (empty(old('radio-player8-gender')) || old('radio-player8-gender') == 'male') ? 'checked' : '' }}>
                                    ชาย
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player8-gender" value="female" {{ (old('radio-player8-gender') == 'female') ? 'checked' : '' }}>
                                    หญิง
                                </label>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <label>ระดับชั้น</label>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player8-grade" value="1" {{ (empty(old('radio-player8-grade')) || old('radio-player8-grade') == '1') ? 'checked' : '' }}>
                                    ม.1
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player8-grade" value="2" {{ (old('radio-player8-grade') == '2') ? 'checked' : '' }}>
                                    ม.2
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player8-grade" value="3" {{ (old('radio-player8-grade') == '3') ? 'checked' : '' }}>
                                    ม.3
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player8-grade" value="4" {{ (old('radio-player8-grade') == '4') ? 'checked' : '' }}>
                                    ม.4
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player8-grade" value="5" {{ (old('radio-player8-grade') == '5') ? 'checked' : '' }}>
                                    ม.5
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="radio-player8-grade" value="6" {{ (old('radio-player8-grade') == '6') ? 'checked' : '' }}>
                                    ม.6
                                </label>
                            </div>
                        </fieldset>
                        <div class="form-group">
                            <label for="input-player8-id">รหัสประจำตัวนักเรียน</label>
                            <input type="text" class="form-control" id="input-player8-id" name="input-player8-id" value="{{ old('input-player8-id') }}">
                        </div>
                        <button type="button" class="btn btn-primary" style="padding: 15; margin-top: 20;" id="previous8">กลับ</button>
                        <button type="submit" class="btn btn-success pull-right" style="padding: 15; margin-top: 20;">สมัคร</button>
                    </div>

                </div>
            </form>
        </div>
    </div>

    <script>
    $(document).ready(function(){
        $(".nav-tabs a").click(function(){
            $(this).tab('show');
        });
    });

    $("#next0").click(function() {
        $('.nav-tabs a[href="#member1"]').trigger('click');
    });
    $("#next1").click(function() {
        $('.nav-tabs a[href="#member2"]').trigger('click');
    });
    $("#next2").click(function() {
        $('.nav-tabs a[href="#member3"]').trigger('click');
    });
    $("#next3").click(function() {
        $('.nav-tabs a[href="#member4"]').trigger('click');
    });
    $("#next4").click(function() {
        $('.nav-tabs a[href="#member5"]').trigger('click');
    });
    $("#next5").click(function() {
        $('.nav-tabs a[href="#member6"]').trigger('click');
    });
    $("#next6").click(function() {
        $('.nav-tabs a[href="#member7"]').trigger('click');
    });
    $("#next7").click(function() {
        $('.nav-tabs a[href="#member8"]').trigger('click');
    });

    $("#previous1").click(function() {
        $('.nav-tabs a[href="#member0"]').trigger('click');
    });
    $("#previous2").click(function() {
        $('.nav-tabs a[href="#member1"]').trigger('click');
    });
    $("#previous3").click(function() {
        $('.nav-tabs a[href="#member2"]').trigger('click');
    });
    $("#previous4").click(function() {
        $('.nav-tabs a[href="#member3"]').trigger('click');
    });
    $("#previous5").click(function() {
        $('.nav-tabs a[href="#member4"]').trigger('click');
    });
    $("#previous6").click(function() {
        $('.nav-tabs a[href="#member5"]').trigger('click');
    });
    $("#previous7").click(function() {
        $('.nav-tabs a[href="#member6"]').trigger('click');
    });

    $('#form-registration').validate({
        ignore: ".ignore",
        invalidHandler: function(e, validator){
            if(validator.errorList.length)
            $('.nav-tabs a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
        }
    });
</script>
@endsection
