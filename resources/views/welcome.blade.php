@extends('layouts.layout')

@section('title', 'OTW To The Champion | Home')

@section('style')
    <style>
        #button-register {
            position: absolute;
            top: 20%;
            width: 20%;
            min-width: 200px;
            height: 65;
            font-size: 23;
            padding: 10;
            border-radius: 10px;
            border-width: 0;
            background-color: #f44336;
            box-shadow: aqua;
            color: white;

            background: -moz-linear-gradient(top, rgba(221, 0, 30, 1) 50%, rgba(208, 0, 0, 1) 51%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(50%, rgba(221, 0, 30, 1)), color-stop(51%, rgba(208, 0, 0, 1))); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top, rgba(221, 0, 30, 1) 50%, rgba(208, 0, 0, 1) 51%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top, rgba(221, 0, 30, 1) 50%, rgba(208, 0, 0, 1) 51%); /* Opera 11.10+ */
            background: -ms-linear-gradient(top, rgba(221, 0, 30, 1) 50%, rgba(208, 0, 0, 1) 51%); /* IE10+ */
            background: linear-gradient(to bottom, rgba(221, 0, 30, 1) 50%, rgba(208, 0, 0, 1) 51%); /* W3C */


            box-shadow: 0px 17px 10px -10px rgba(0,0,0,0.4);
            cursor: pointer;
            transition: all ease-in-out 300ms;
        }

        #button-register:hover {
            box-shadow: 0px 37px 20px -20px rgba(0,0,0,0.2);
            transform: translate(0px, -10px) scale(1.2);
        }

        #div_rov {
            animation:move 1s infinite ease-in-out;
        }

        @keyframes move{
            0%{
                transform: scale(1) rotate(0deg);
            }
            20%{
                transform: scale(1.01) rotate(0deg);
            }
            60%{
                transform: scale(1.01) rotate(0deg);
            }
            80%{
                transform: scale(1) rotate(0deg);
            }
            100%{
                transform: scale(1) rotate(0deg);
            }
        }

        #div_dota2 {
            animation:move2 1s infinite ease-in-out;
        }

        @keyframes move2{
            0%{
                transform: scale(1) rotate(0deg);
            }
            20%{
                transform: scale(1) rotate(0deg);
            }
            40%{
                transform: scale(1.01) rotate(0deg);
            }
            80%{
                transform: scale(1.01) rotate(0deg);
            }
            100%{
                transform: scale(1) rotate(0deg);
            }
        }

    </style>
@endsection


@section('content')
    <div class="col-md-6" style="background-image: url('{{ asset('assets/images/Register_Home_01.jpg')}}'); background-size: cover; background-repeat: repeat; background-position: center center; height: 100%;">
        <div style="height: 100%; display: flex; align-items: center;" id="div_rov">
            <div style="margin: auto;">
                <img src="{{ asset('assets/images/Logo_RoV_PartII.png')}}" style="max-width: 450px; width: 90%; vertical-align: middle; display: block; margin: auto;"></img>
                <img src="{{ asset('assets/images/word_otc.png')}}" style="max-width: 500px; width: 90%; vertical-align: middle; display: block; margin: auto;"></img>
            </div>
        </div>
    </div>

    <div class="col-md-6" style="background-image: url('{{ asset('assets/images/Register_Home_02.jpg')}}'); background-size: cover; background-repeat: repeat; background-position: center center; height: 100%;">
        <div style="height: 100%; display: flex; align-items: center;" id="div_dota2">
            <div style="margin: auto;">
                <img src="{{ asset('assets/images/logodota.png')}}" style="max-width: 450px; width: 65%; vertical-align: middle; display: block; margin: auto;"></img>
                <img src="{{ asset('assets/images/dota_text.png')}}" style="max-width: 300px; width: 90%; vertical-align: middle; display: block; margin: auto;"></img>
            </div>
        </div>
    </div>

    <!-- <img src="http://www.bkkdeveloper.com/img/RoVPoster_webbg.jpg" style="object-fit: contain; height: 100%"> -->
    <div style="display: flex; justify-content: center;">



    </div>

    <script>
        $("#div_rov").css('cursor','pointer');
        $("#div_rov").click(function() {
            window.location = '{{url('register-free')}}';
        });

        $("#div_dota2").css('cursor','pointer');
        $("#div_dota2").click(function() {
            window.location = '{{url('register-d2')}}';
        });
    </script>
@endsection
