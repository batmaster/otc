@extends('layouts.layout')

@section('title', 'OTW To The Champion Dota2 | Notice')

@section('style')
    <style>
        body {
            background-image: url('{{ asset('assets/images/bg.jpg')}}');
            background-size: cover;
            background-repeat: repeat;
            background-position: center center;
        }
        h3 {
            color: #f2aa3a;
            background-color: #2b333dee;
            padding: 20px;
            /* text-shadow: 1px 0px 5px #f2aa3a66; */
        }
        p {
            color: #f2aa3a;
            padding: 20px;
            font-size: 18px;
            /* text-shadow: 1px 0px 15px #f2aa3a66; */

        }
    </style>
@endsection


@section('content')
<div class="container">
    <div style="background-color: #353e4899; min-height: 80%; box-shadow: 1px 1px 20px;">
        <h3>เปิดรับสมัครการแข่งขัน OTW To The Champion 2018 (Dota 2)!!!</h3>
        <p style="text-indent: 50px;">เปิดรับสมัครการแข่งขัน OTW To The Champion 2018 (Dota 2)!!! OTW เปิดโอกาสให้น้อง ๆ เยาวชนและประชาชนทั่วไปไม่จำกัดอายุ ทั่วประเทศ มาเข้าร่วมการแข่งขันเกม Dota 2 เพื่อค้นหาแชมป์ พร้อมชิงเงินรางวัลรวม 500,000 บาท!!!
 ซึ่งเปิดรับสมัครถึง 512 ทีม !!! เป็นศึกการแข่งขันระหว่างทีมต่างๆ  เพื่อเฟ้นหาสุดยอดทีมแชมป์ของระดับประเทศ เริ่มเปิดรับสมัครแล้วตั้งแต่วันนี้ ถึง 18 เมษายน 2561
        </p>

        <h3>ตารางการแข่งขัน</h3>
        <p>วันที่ 19 เมษายน 2561 ประกาศสายการแข่งขันที่เพจ Facebook : <a href="https://www.facebook.com/OTWDota2Esports">OTWDota2Esports</a></p>

        <h3>รอบออนไลน์</h3>
        <p>
            - เริ่มการแข่งขันวันที่ 20 เม.ษ.<br>
            - เริ่มแข่งเวลา 19.00 น. ของทุกวัน สายได้ไม่เกิน 30 นาที<br>
            - มีสุ่มถ่ายทอดสดการแข่งขันทางเพจ Facebook : <a href="https://www.facebook.com/OTWDota2Esports">OTWDota2Esports</a><br>
            - เลื่อนการแข่งขันได้ แต่ต้องไม่ข้ามวัน และไม่เกิน 24.00 น. ของวันแข่งขัน โดยให้ตกลงเสร็จสิ้นก่อนเวลา 17.00 น. ถ้าตกลงกันไม่ได้ ให้ยึดเวลากลางคือ 19.00 น. เป็นที่ตั้ง<br>
            - หากมาไม่ทันกำหนดเวลาแข่งขันตามที่ตกลงเกิน 30 นาที ทีมที่มารอสามารถรายงานขอชนะบายได้<br>
            - หากตกลงเวลากันไม่ได้ภายในวันแข่งขัน และไม่มีการแข่งขันเกิดขึ้น จะถือว่าแพ้บายทั้ง 2 ทีม<br>
            - ไอดีที่ไม่ได้ลงทะเบียนไม่สามารถเข้าร่วมการแข่งขันได้ หากตรวจจะถูกตัดสิทธิ์ทันที
        </p>

        <h3>รอบออฟไลน์</h3>
        <p>
            สถานที่และเวลาการแข่งขันจะแจ้งทีหลัง<br>* วันและเวลาการแข่งขันอาจมีการเปลี่ยนแปลง ทีมที่เข้าแข่งสามารถติดตามรายละเอียดการแข่งขันที่เพจ Facebook: <a href="https://www.facebook.com/OTWDota2Esports">OTWDota2Esports</a>
        </p>
    </div>
</div>

    <script>
        $("#button-register").click(function() {
            window.location = '{{url('register')}}';
        });
    </script>
@endsection
