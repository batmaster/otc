<html>
<head>
    @yield('head')

    <!-- For Google -->
    <!--
    <meta name="description" content="เปิดโอกาสให้น้องๆ ระดับมัธยมศึกษาหรือเทียบเท่าในเขตพื้นที่กรุงเทพมหานครและปริมณฑล ชิงทุนการศึกษามูลค่ารวมกว่า 1,000,000 บาท!!! ตั้งแต่วันนี้ ถึง 25 มีนาคม 2561 เวลา 23.59น." />
    <meta name="keywords" content="" />
    <meta name="author" content="BKK Developer" />
    <meta name="copyright" content="BKK Developer" />
    <meta name="application-name" content="OTC" />
    -->

    <!-- For Facebook -->
    <!--
    <meta property="og:title" content="OTW eSports | โครงการการแข่งขันกีฬาอิเล็กทรอนิกส์สำหรับเยาวชนไทย" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="https://otc.bkkdeveloper.com/assets/images/og_image.jpg" />
    <meta property="og:url" content="https://otc.bkkdeveloper.com" />
    <meta property="og:description" content="โครงการการแข่งขันกีฬา eSports สำหรับเยาวชนไทยรุ่นใหม่ ในระดับชั้นมัธยมศึกษาหรือเทียบเท่า" />
    -->

    <!-- For Twitter -->
    <!--
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="เปิดรับสมัครการแข่งขัน OTW To The Champion 2018 (Part 2)!!!" />
    <meta name="twitter:description" content="เปิดโอกาสให้น้องๆ ระดับมัธยมศึกษาหรือเทียบเท่าในเขตพื้นที่กรุงเทพมหานครและปริมณฑล ชิงทุนการศึกษามูลค่ารวมกว่า 1,000,000 บาท!!! ตั้งแต่วันนี้ ถึง 25 มีนาคม 2561 เวลา 23.59น." />
    <meta name="twitter:image" content="https://otc.bkkdeveloper.com/assets/images/og_image.jpg" />
    -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-111860774-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-111860774-1');
    </script>

    <title>OTW To The Champion | Confirm</title>
    <link rel="shortcut icon" type="image/png" href="http://www.bkkdeveloper.com/img/logo.png"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.min.js"></script>

    <link rel="stylesheet" type="text/css" media="screen" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>


    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">


    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>

    <script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">

    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <style>
        nav {
            /* height: 64px; */
            padding: 0% 10% !important;
            background: -moz-linear-gradient(top, rgba(17, 23, 29, 1) 0%, rgba(40, 45, 55, 1) 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgba(17, 23, 29, 1)), color-stop(100%, rgba(40, 45, 55, 1))); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top, rgba(17, 23, 29, 1) 0%, rgba(40, 45, 55, 1) 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top, rgba(17, 23, 29, 1) 0%, rgba(40, 45, 55, 1) 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(top, rgba(17, 23, 29, 1) 0%, rgba(40, 45, 55, 1) 100%); /* IE10+ */
            background: linear-gradient(to bottom, rgba(17, 23, 29, 1) 0%, rgba(40, 45, 55, 1) 100%); /* W3C */
        }

        navbar-default>ul>li>a {
            color: white !important;
        }

        .navbar-default .navbar-nav>.active>a, .navbar-default .navbar-nav>.active>a:hover, .navbar-default .navbar-nav>.active>a:focus {
            color: red !important;
            background-color: transparent;
        }

        .navbar-default a {
            transition: 0.3s;
        }

        .navbar-default a:hover {
            color: red !important;
        }

        .navbar {
            margin-bottom: 0;
            border-radius: 0;
            padding-top: 10px !important;
        }

        .navbar-default {
            border-color: transparent;
        }

        .error {
            color: red;
        }

    </style>

    @yield('style')
</head>
<body>
    <nav class="navbar navbar-default" style="z-index: 100">

        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>

              <img src="https://bkkdeveloper.com/img/otwlogo100px.png" />
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                  <li class="nav-item  {!! Request::is('welcome') || Request::is('/') ? 'active' : '' !!}">
                    <a class="nav-link" href="welcome">หน้าหลัก <span class="sr-only">(current)</span></a>
                  </li>
                  <li role="presentation" class="dropdown {!! Request::is('notice') || Request::is('rule') || Request::is('register') || Request::is('register-free') || Request::is('done') || Request::is('done-free') ? 'active' : '' !!}">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                          ROV Part2
                      </a>
                      <ul class="dropdown-menu">
                          <li class="{!! Request::is('notice') ? 'active' : '' !!}"><a href="notice">ประกาศ</a></li>
                          <li class="{!! Request::is('rule') ? 'active' : '' !!}"><a href="rule">กติกา</a></li>
                          <li role="separator" class="divider"></li>
                          <li class="{!! Request::is('register-free') ? 'active' : '' !!}"><a href="register-free">ลงทะเบียน</a></li>
                      </ul>
                  </li>

                  <li role="presentation" class="dropdown {!! Request::is('notice-d2') || Request::is('rule-d2') || Request::is('how-to-get-steam') || Request::is('register-d2') || Request::is('done-d2') ? 'active' : '' !!}">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                          DOTA2
                      </a>
                      <ul class="dropdown-menu">
                          <li class="{!! Request::is('notice-d2') ? 'active' : '' !!}"><a href="notice-d2">ประกาศ</a></li>
                          <li class="{!! Request::is('rule-d2') ? 'active' : '' !!}"><a href="rule-d2">กติกา</a></li>
                          <li class="{!! Request::is('how-to-get-steam') ? 'active' : '' !!}"><a href="how-to-get-steam">วิธีการรับ STEAM ID</a></li>
                          <li role="separator" class="divider"></li>
                          <li class="{!! Request::is('register-d2') || Request::is('done-d2') ? 'active' : '' !!}"><a href="register-d2">ลงทะเบียน</a></li>
                      </ul>
                  </li>


                  <li class="nav-item {!! Request::is('#') ? 'active' : '' !!}" style="display: none">
                    <a class="nav-link" href="#">ตารางแข่งขัน</a>
                  </li>
                  <li class="nav-item {!! Request::is('#') ? 'active' : '' !!}" style="display: none">
                    <a class="nav-link" href="#">ติดต่อทีมงาน</a>
                  </li>
              </ul>

            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
    </nav>

    @yield('content')
</body>
</html>
