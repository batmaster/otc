@extends('layouts.layout')

@section('title', 'OTW To The Champion | Confirmation')

@section('style')

@endsection

@section('content')
    <div style="background-image: url('{{ asset('assets/images/RoVHome_01.jpg')}}'); background-repeat: no-repeat; background-size: contain; background-color: #D6D0E1; padding-top: 10%; padding-bottom: 20px">

        <div class="container" style="background: white; padding: 40; max-width: 800;position: relative; left: 0; top: 0; box-shadow: 0px -10px #C52431;">
            <h2>ON THE WAY TO THE CHAMPION</h2>

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <p>เปิดโอกาสให้น้องๆ ระดับมัธยมศึกษาหรือเทียบเท่า ที่มีอายุไม่เกิน 18 ปี มาเข้าร่วมการแข่งขันเกม Rov เพื่อค้นหาแชมป์หนึ่งเดียวของโรงเรียนในเขตพื้นที่กรุงเทพมหานครและปริมณฑล พร้อมชิงทุนการศึกษามูลค่ารวมกว่า 1,000,000 บาท!!!  และได้เซ็นสัญญาเป็นนักกีฬา eSports อย่างเต็มตัว ภายใต้บริษัท บีเคเค ดีเวลลอปเปอร์ จำกัด 1 ปี พร้อมเงินเดือน 20,000 บาท ต่อคน!!!
            </p>

            <p><h5>*โปรดติดตามรายละเอียดการแข่งขันได้ที่เพจ</h5>
                Facebook : <b>OTWesports</b><br>
                Line : <b>@otwjuniorleague</b> (มี@ด้วย)
            </p>

            <form method="POST" action="{{ url('confirm') }}" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <h3 style="margin-top: 50px; background-color: #9C222F; padding: 20; color: white">กรุณากรอกข้อมูลยืนยันการชำระเงิน</h3>
                <div class="form-group">
                    <label for="input-ref">หมายเลข Reference</label>
                    <input type="text" class="form-control form-control-sm" id="input-ref" name="input-ref" value="{{ empty(old('input-ref')) ? ($registration2 == null ? "" : $registration2->ref) : old('input-ref') }}" required>
                </div>
                <div class="form-group">
                    <label>ทีม <img style="width: 32px; display: none"></label>
                    <input type="text" class="form-control form-control-sm" id="input-team" name="input-team" value="{{ empty(old('input-team')) ? ($registration2 == null ? "" : $registration2->team) : old('input-team') }}" readonly required>
                </div>
                <div class="form-group">
                    <label for="input-amount">ยอดเงินที่โอน</label>
                    <input type="number" min="0" step="0.01" class="form-control form-control-sm" id="input-amount" name="input-amount" value="{{ old('input-amount') }}" required>
                </div>
                <div class="form-group">
                    <label for="input-datetime">วันที่-เวลาที่โอน</label>
                    <div class="form-group">
                        <div class='input-group date'>
                            <input type='text' class="form-control" id="input-datetime" name="input-datetime"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="file-image-team">ธนาคารที่โอนเข้า</label>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="radio-banks" id="radio-bank1" value="SCB" {{ (empty(old('radio-banks')) || old('radio-banks') == 'SCB') ? 'checked' : '' }}>
                        &nbsp
                        <label class="form-check-label" for="radio-bank1" style="vertical-align: top;">
                            <p>
                                <img src="{{ asset('assets/images/scb.png')}}">&nbsp ธนาคารไทยพาณิชย์ สาขาโลตัส ทาวน์ อิน ทาวน์ ประเภทกระแสรายวัน
                                <br>ชื่อบัญชี บริษัท บีเคเค ดีเวลอปเปอร์ จำกัด
                                <br>เลขที่บัญชี 468-066918-9
                            </p>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="radio-banks" id="radio-bank2" value="KBANK" {{ (old('radio-banks') == 'KBANK') ? 'checked' : '' }}>
                        &nbsp
                        <label class="form-check-label" for="radio-bank2" style="vertical-align: top;">
                            <p>
                                <img src="{{ asset('assets/images/kbank.png')}}">&nbsp ธนาคารกสิกรไทย สาขาโลตัส ทาวน์ อิน ทาวน์ ประเภทกระแสรายวัน
                                <br>ชื่อบัญชี บริษัท บีเคเค ดีเวลอปเปอร์ จำกัด
                                <br>เลขที่บัญชี 032-3-33512-5
                            </p>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="input-from-bank">ธนาคารตันทางที่โอน</label>
                            <input type="text" class="form-control form-control-sm" id="input-from-bank" name="input-from-bank" value="{{ old('input-from-bank') }}" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="input-from-bank-branch">สาขา</label>
                            <input type="text" class="form-control form-control-sm" id="input-from-bank-branch" name="input-from-bank-branch" value="{{ old('input-from-bank-branch') }}" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="file-image-confirm">หลักฐานการโอนเงิน</label>
                    <input type="file" class="form-control-file" id="file-image-confirm" name="file-image-confirm" aria-describedby="fileHelp">
                </div>
                <div class="form-group">
                    <label for="input-note">หมายเหตุ</label>
                    <textarea class="form-control" id="input-note" name="input-note">{{ old('input-note') }}</textarea>
                </div>



                <h3 style="margin-top: 50px; background-color: #9C222F; padding: 20; color: white">
                    เปิดรับสมัครแล้วตั้งแต่วันที่ 7 กุมภาพันธ์ - 25 มีนาคม 2561 เวลา 23.59 น.
                </h3>

                <button type="submit" class="btn btn-primary" style="padding: 15; margin-top: 20;">แจ้งชำระเงิน</button>
            </form>
        </div>
    </div>

    <script>

    $(function () {
        function getTeam() {
            if ($("#input-ref").val() != "") {
                $.ajax({
                    url: "{{ url('/teams') }}",
                    dataType: 'json',
                    data: {
                        ref: $("#input-ref").val()
                    }
                }).done(function(e) {
                    // console.log(e)
                    if (e.team != undefined) {
                        $("#input-team").val(e.team + " สมัครเมื่อ " + e.created_at);
                    }

                }).error(function(e) {
                    // console.log(e);
                    $("#input-team").val("");
                });
            }
        }

        $("#input-ref").keyup(function() {
            getTeam();
        });

        $("#input-ref").change(function() {
            getTeam();
        });






        $("#input-datetime").datetimepicker({
            date: "{{ old('input-datetime') }}",
            format: 'DD/MM/YYYY HH:mm'
        });
        $("#input-datetime").focus(function() {
            $("#input-datetime").datetimepicker("show");
        });

        // $('#input-datetime').on('apply.daterangepicker', function(ev, picker) {
        //     // $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
        //     //
        //     // table.columns(1).search($('#daterange').val()).draw();
        // });
        //
        // $('#input-datetime').on('cancel.daterangepicker', function(ev, picker) {
        //     // $(this).val('');
        //     //
        //     // table.columns(1).search($('#daterange').val()).draw();
        // });
    });
    </script>
@endsection
