@extends('layouts.app')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">Dashboard</div>

        <div class="panel-body">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif

            <table id="example" class="display" width="100%"></table>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">
    var dataSet = {!! $r !!}
    $(document).ready(function() {
        var buttonCommon = {
            exportOptions: {
                format: {
                    body: function ( data, row, column, node ) {

                        if (column === 4 || column === 5) {
                            return $($.parseHTML(data)).find("img").attr("src");
                        }
                        else {
                            if ( typeof data !== 'string' ) {
                                return data;
                            }

                            data = data.replace( /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi, '' );

                            data = data.replace( /<[^>]*>/g, '' );
                            data = data.replace( /^\s+|\s+$/g, '' );
                            data = data.replace( /\n/g, ' ' );

                            return data;
                        }
                    }
                }
            }
        };

        $('#example').DataTable({
//            fixedHeader: true,
            scrollX: true,
            data: dataSet,
            // dom: 'Bfrtip',
            sDom: '<"top"Bl>frt<"bottom"ip><"clear">',
            buttons: [
                // 'excel'
                $.extend( true, {}, buttonCommon, {
                    extend: 'excelHtml5'
                } ),
            ],
            columns: [
                { title: "Id", data: "id" },
                { title: "Date", data: "created_at" },

                { title: "School", data: "school" },
                { title: "Team Name", data: "team" },
                { title: "Team Image", data: "image_team" },
                { title: "Team Logo", data: "image_logo" },

                { title: "Player1 Name", data: "player1_name" },
                { title: "Player1 Phone", data: "player1_phone" },
                { title: "Player1 Line", data: "player1_line" },
                { title: "Player1 Gender", data: "player1_gender" },
                { title: "Player1 Grade", data: "player1_grade" },
                { title: "Player1 id", data: "player1_id" },

                { title: "Player2 Name", data: "player2_name" },
                { title: "Player2 Phone", data: "player2_phone" },
                { title: "Player2 Gender", data: "player2_gender" },
                { title: "Player2 Grade", data: "player2_grade" },
                { title: "Player2 id", data: "player2_id" },

                { title: "Player3 Name", data: "player3_name" },
                { title: "Player3 Phone", data: "player3_phone" },
                { title: "Player3 Gender", data: "player3_gender" },
                { title: "Player3 Grade", data: "player3_grade" },
                { title: "Player3 id", data: "player3_id" },

                { title: "Player4 Name", data: "player4_name" },
                { title: "Player4 Phone", data: "player4_phone" },
                { title: "Player4 Gender", data: "player4_gender" },
                { title: "Player4 Grade", data: "player4_grade" },
                { title: "Player4 id", data: "player4_id" },

                { title: "Player5 Name", data: "player5_name" },
                { title: "Player5 Phone", data: "player5_phone" },
                { title: "Player5 Gender", data: "player5_gender" },
                { title: "Player5 Grade", data: "player5_grade" },
                { title: "Player5 id", data: "player5_id" },

                { title: "Player6 Name", data: "player6_name" },
                { title: "Player6 Phone", data: "player6_phone" },
                { title: "Player6 Gender", data: "player6_gender" },
                { title: "Player6 Grade", data: "player6_grade" },
                { title: "Player6 id", data: "player6_id" },
            ],
            columnDefs: [
                {
                    targets: 3,
                    render: function(data, type, row) {
                        return "<b>" + data + "</b>";
                    }
                },
                {
                    targets: 4,
                    render: function(data, type, row) {
                        var url = 'https://otc.bkkdeveloper.com/' + data
                        return '\
                        <a href="' + url + '" target="_blank">\
                            <img style="height: 220px" border="0" align="center"  src="' + url + '"/>\
                        </a>\
                        ';
                    }
                },
                {
                    targets: 5,
                    render: function(data, type, row) {
                        if (data == '') {
                            return '';
                        }

                        var url = 'https://otc.bkkdeveloper.com/' + data
                        return '\
                        <a href="' + url + '" target="_blank">\
                            <img style="height: 220px" border="0" align="center"  src="' + url + '"/>\
                        </a>\
                        ';
                    }
                }
            ]
        });
    });
</script>
@endpush
