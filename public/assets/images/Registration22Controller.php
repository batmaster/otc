<?php

namespace App\Admin\Controllers;

use App\Registration22;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class Registration22Controller extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Registration22::class, function (Grid $grid) {

            $grid->id('ID')->sortable();

            $grid->column('ticket_code', 'Ticket Code');

            $grid->column('image_team', 'Team Image')->display(function ($path) {
                return 'http://otc.bkkdeveloper.com/uploads/'. $path;
            })->image();

            $grid->column('image_logo', 'Team Logo')->display(function ($path) {
                return 'http://otc.bkkdeveloper.com/uploads/'. $path;
            })->image();
            $grid->column('team', 'Team Name');
            $grid->column('player1_name', 'Player 1 Name');
            $grid->column('player1_phone', 'Player 1 Phone');
            $grid->column('player1_line', 'Player 1 Line ID');
            $grid->column('player1_fb', 'Player 1 Facebook');
            $grid->column('player1_email', 'Player 1 Email');

            $grid->created_at()->sortable();
            $grid->updated_at()->sortable();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Registration22::class, function (Form $form) {

            $genders = [
                'male' => 'ชาย',
                'female' => 'หญิง'
            ];

            $grades = [
                1 => 'ม.1',
                2 => 'ม.2',
                3 => 'ม.3',
                4 => 'ม.4',
                5 => 'ม.5',
                6 => 'ม.6'
            ];

            $form->tab('ข้อมูลทั่วไป', function ($form) use ($genders, $grades) {

                $form->display('id', 'ID');

                $form->display('ticket_code', 'Ticket Code');
                $form->display('school', 'School');
                $form->display('team', 'Team Name');
                $form->image('image_team', 'Team Image')->name(function ($path) {
                    return 'https://otc.bkkdeveloper.com/'. $path;
                });
                $form->image('image_logo', 'Team Logo')->name(function ($path) {
                    return 'https://otc.bkkdeveloper.com/'. $path;
                });

                $form->display('created_at', 'Created At');
                $form->display('updated_at', 'Updated At');

            })->tab('คนที่ 1 (หัวหน้าทีม)', function ($form) use ($genders, $grades) {
                $form->text('player1_name');
                $form->text('player1_phone');
                $form->text('player1_line');
                $form->text('player1_fb');
                $form->text('player1_email');
                // $form->display('player1_gender');
                // $form->display('player1_grade');
                $form->select('player1_gender', 'Player 1 Gender')->options($genders);
                $form->select('player1_grade', 'Player 1 Grade')->options($grades);
                $form->text('player1_id');

            })->tab('คนที่ 2', function ($form) use ($genders, $grades) {
                $form->text('player2_name');
                $form->text('player2_phone');
                $form->text('player2_fb');
                $form->text('player2_email');
                // $form->display('player2_gender');
                // $form->display('player2_grade');
                $form->select('player2_gender', 'Player 2 Gender')->options($genders);
                $form->select('player2_grade', 'Player 2 Grade')->options($grades);
                $form->text('player2_id');

            })->tab('คนที่ 3', function ($form) use ($genders, $grades) {
                $form->text('player3_name');
                $form->text('player3_phone');
                $form->text('player3_fb');
                $form->text('player3_email');
                // $form->display('player3_gender');
                // $form->display('player3_grade');
                $form->select('player3_gender', 'Player 3 Gender')->options($genders);
                $form->select('player3_grade', 'Player 3 Grade')->options($grades);
                $form->text('player3_id');

            })->tab('คนที่ 4', function ($form) use ($genders, $grades) {
                $form->text('player4_name');
                $form->text('player4_phone');
                $form->text('player4_fb');
                $form->text('player4_email');
                // $form->display('player4_gender');
                // $form->display('player4_grade');
                $form->select('player4_gender', 'Player 4 Gender')->options($genders);
                $form->select('player4_grade', 'Player 4 Grade')->options($grades);
                $form->text('player4_id');

            })->tab('คนที่ 5', function ($form) use ($genders, $grades) {
                $form->text('player5_name');
                $form->text('player5_phone');
                $form->text('player5_fb');
                $form->text('player5_email');
                // $form->display('player5_gender');
                // $form->display('player5_grade');
                $form->select('player5_gender', 'Player 5 Gender')->options($genders);
                $form->select('player5_grade', 'Player 5 Grade')->options($grades);
                $form->text('player5_id');

            })->tab('คนที่ 6', function ($form) use ($genders, $grades) {
                $form->text('player6_name');
                $form->text('player6_phone');
                $form->text('player6_fb');
                $form->text('player6_email');
                // $form->display('player6_gender');
                // $form->display('player6_grade');
                $form->select('player6_gender', 'Player 6 Gender')->options($genders);
                $form->select('player6_grade', 'Player 6 Grade')->options($grades);
                $form->text('player6_id');

            })->tab('คนที่ 7', function ($form) use ($genders, $grades) {
                $form->text('player7_name');
                $form->text('player7_phone');
                $form->text('player7_fb');
                $form->text('player7_email');
                // $form->display('player7_gender');
                // $form->display('player7_grade');
                $form->select('player7_gender', 'Player 7 Gender')->options($genders);
                $form->select('player7_grade', 'Player 7 Grade')->options($grades);
                $form->text('player7_id');

            })->tab('คนที่ 8', function ($form) use ($genders, $grades) {
                $form->text('player8_name');
                $form->text('player8_phone');
                $form->text('player8_fb');
                $form->text('player8_email');
                // $form->display('player8_gender');
                // $form->display('player8_grade');
                $form->select('player8_gender', 'Player 8 Gender')->options($genders);
                $form->select('player8_grade', 'Player 8 Grade')->options($grades);
                $form->text('player8_id');

            });


        });
    }
}
