<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registration22 extends Model
{
    public function getPeopleAttribute() {
        $a = 0;
        if ($this->player1_name != null && $this->player1_name != "") {
            $a += 1;
        }
        if ($this->player2_name != null && $this->player2_name != "") {
            $a += 1;
        }
        if ($this->player3_name != null && $this->player3_name != "") {
            $a += 1;
        }
        if ($this->player4_name != null && $this->player4_name != "") {
            $a += 1;
        }
        if ($this->player5_name != null && $this->player5_name != "") {
            $a += 1;
        }
        if ($this->player6_name != null && $this->player6_name != "") {
            $a += 1;
        }
        if ($this->player7_name != null && $this->player7_name != "") {
            $a += 1;
        }
        if ($this->player8_name != null && $this->player8_name != "") {
            $a += 1;
        }

        return $a;
    }

    public function getReceiptAttribute() {
        return 'OTC' . sprintf('%04d', $this->id);
    }
}
