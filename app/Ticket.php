<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    public function generateCode() {
        return 'OTW' . sprintf('%04d', $this->id) . (($this->id * $this->id) % 10);
    }

    public function generateUrl() {
        return md5($this->generateCode());
    }

    public function registration22() {
        return $this->hasOne('App\Registration22', 'ticket_code', 'code');
    }
}
