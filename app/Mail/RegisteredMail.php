<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisteredMail extends Mailable
{
    use Queueable, SerializesModels;

    public $registration22;

    public function __construct($registration22)
    {
        $this->registration22 = $registration22;
    }

    public function build()
    {
        return $this->subject("[OTC Part2] ได้รับข้อมูลการสมัครแล้ว")->view('emails.registered', compact('registration22'));
    }
}
