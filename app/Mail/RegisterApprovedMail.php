<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterApprovedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $payment;
    public $registration2;

    public function __construct($payment, $registration2)
    {
        $this->payment = $payment;
        $this->registration2 = $registration2;
    }

    public function build()
    {
        return $this->subject("[OTC Part2] คุณได้ผ่านการสมัครเข้าแข่งขันสำเร็จแล้ว")->view('emails.register_approved', compact('payment, registration2'));
    }
}
