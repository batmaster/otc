<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PaymentMail extends Mailable
{
    use Queueable, SerializesModels;

    public $payment;
    public $registration2;

    public function __construct($payment, $registration2)
    {
        $this->payment = $payment;
        $this->registration2 = $registration2;
    }

    public function build()
    {
        return $this->subject("[OTC Part2] ได้รับข้อมูลการแจ้งชำระเงินเรียบร้อยแล้ว")->view('emails.payment', compact('payment, registration2'));
    }
}
