<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registration2 extends Model
{
    public function generateRef() {
        return 'OTW' . sprintf('%04d', $this->id) . (($this->id * $this->id) % 10);
    }

    public function payments() {
        return $this->hasOne('App\Payment', 'ref', 'ref');
    }

    public function getPeopleAttribute() {
        $a = 0;
        if ($this->player1_name != null && $this->player1_name != "") {
            $a += 1;
        }
        if ($this->player2_name != null && $this->player2_name != "") {
            $a += 1;
        }
        if ($this->player3_name != null && $this->player3_name != "") {
            $a += 1;
        }
        if ($this->player4_name != null && $this->player4_name != "") {
            $a += 1;
        }
        if ($this->player5_name != null && $this->player5_name != "") {
            $a += 1;
        }
        if ($this->player6_name != null && $this->player6_name != "") {
            $a += 1;
        }
        if ($this->player7_name != null && $this->player7_name != "") {
            $a += 1;
        }
        if ($this->player8_name != null && $this->player8_name != "") {
            $a += 1;
        }

        return $a;
    }
}
