<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use DB;
use Mail;

use App\RegistrationFree;
use App\Payment;
use App\Ticket;
use App\Mail\RegisteredMail;
use App\Mail\PaymentMail;
use App\Mail\RegisterApprovedMail;

class RegisterTeamFreeController extends Controller
{

    public function registerPage() {
        return view('register-free');
    }

    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'input-school' => 'required',
            'input-team' => 'required',
            'file-image-team' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'file-image-logo' => 'image|mimes:jpeg,png,jpg,gif,svg',

            'input-player1-name' => 'required',
            'input-player1-phone' => 'required',
            'input-player1-fb' => 'required',
            'input-player1-line' => 'required',
            'input-player1-email' => 'required',
            'radio-player1-gender' => 'required',
            'radio-player1-grade' => 'required',
            'input-player1-id' => 'required',

            'input-player2-name' => 'required',
            'input-player2-phone' => 'required',
            'input-player2-fb' => 'required',
            'input-player2-email' => 'required',
            'radio-player2-gender' => 'required',
            'radio-player2-grade' => 'required',
            'input-player2-id' => 'required',

            'input-player3-name' => 'required',
            'input-player3-phone' => 'required',
            'input-player3-fb' => 'required',
            'input-player3-email' => 'required',
            'radio-player3-gender' => 'required',
            'radio-player3-grade' => 'required',
            'input-player3-id' => 'required',

            'input-player4-name' => 'required',
            'input-player4-phone' => 'required',
            'input-player4-fb' => 'required',
            'input-player4-email' => 'required',
            'radio-player4-gender' => 'required',
            'radio-player4-grade' => 'required',
            'input-player4-id' => 'required',

            'input-player5-name' => 'required',
            'input-player5-phone' => 'required',
            'input-player5-fb' => 'required',
            'input-player5-email' => 'required',
            'radio-player5-gender' => 'required',
            'radio-player5-grade' => 'required',
            'input-player5-id' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('welcome')->withErrors($validator)->withInput();
        }


        DB::beginTransaction();
        try {
            $destinationPath = public_path('/uploads/images');

            $image = $request->file('file-image-team');
            if ($image) {
                $input['image_team_name'] = time().'image_team_name.'.$image->getClientOriginalExtension();
                $image->move($destinationPath, $input['image_team_name']);
            }
            else {
                $input['image_team_name'] = '';
            }

            $image2 = $request->file('file-image-logo');
            if ($image2) {
                $input['image_logo_name'] = time().'image_logo_name.'.$image2->getClientOriginalExtension();
                $image2->move($destinationPath, $input['image_logo_name']);
            }
            else {
                $input['image_logo_name'] = '';
            }

            $r = new RegistrationFree();
            $r->school = $request->input('input-school');
            $r->team = $request->input('input-team');
            $r->image_team = $input['image_team_name'] != '' ? 'images/' . $input['image_team_name'] : '';
            $r->image_logo = $input['image_logo_name'] != '' ? 'images/' . $input['image_logo_name'] : '';

            $r->player1_name = $request->input('input-player1-name');
            $r->player1_phone = $request->input('input-player1-phone');
            $r->player1_fb = $request->input('input-player1-fb');
            $r->player1_line = $request->input('input-player1-line');
            $r->player1_email = $request->input('input-player1-email');
            $r->player1_gender = $request->input('radio-player1-gender');
            $r->player1_grade = $request->input('radio-player1-grade');
            $r->player1_id = $request->input('input-player1-id');

            $r->player2_name = $request->input('input-player2-name');
            $r->player2_phone = $request->input('input-player2-phone');
            $r->player2_fb = $request->input('input-player2-fb');
            $r->player2_email = $request->input('input-player2-email');
            $r->player2_gender = $request->input('radio-player2-gender');
            $r->player2_grade = $request->input('radio-player2-grade');
            $r->player2_id = $request->input('input-player2-id');

            $r->player3_name = $request->input('input-player3-name');
            $r->player3_phone = $request->input('input-player3-phone');
            $r->player3_fb = $request->input('input-player3-fb');
            $r->player3_email = $request->input('input-player3-email');
            $r->player3_gender = $request->input('radio-player3-gender');
            $r->player3_grade = $request->input('radio-player3-grade');
            $r->player3_id = $request->input('input-player3-id');

            $r->player4_name = $request->input('input-player4-name');
            $r->player4_phone = $request->input('input-player4-phone');
            $r->player4_fb = $request->input('input-player4-fb');
            $r->player4_email = $request->input('input-player4-email');
            $r->player4_gender = $request->input('radio-player4-gender');
            $r->player4_grade = $request->input('radio-player4-grade');
            $r->player4_id = $request->input('input-player4-id');

            $r->player5_name = $request->input('input-player5-name');
            $r->player5_phone = $request->input('input-player5-phone');
            $r->player5_fb = $request->input('input-player5-fb');
            $r->player5_email = $request->input('input-player5-email');
            $r->player5_gender = $request->input('radio-player5-gender');
            $r->player5_grade = $request->input('radio-player5-grade');
            $r->player5_id = $request->input('input-player5-id');

            $r->player6_name = $request->input('input-player6-name');
            $r->player6_phone = $request->input('input-player6-phone');
            $r->player6_fb = $request->input('input-player6-fb');
            $r->player6_email = $request->input('input-player6-email');
            $r->player6_gender = $request->input('radio-player6-gender');
            $r->player6_grade = $request->input('radio-player6-grade');
            $r->player6_id = $request->input('input-player6-id');

            $r->player7_name = $request->input('input-player7-name');
            $r->player7_phone = $request->input('input-player7-phone');
            $r->player7_fb = $request->input('input-player7-fb');
            $r->player7_email = $request->input('input-player7-email');
            $r->player7_gender = $request->input('radio-player7-gender');
            $r->player7_grade = $request->input('radio-player7-grade');
            $r->player7_id = $request->input('input-player7-id');

            $r->player8_name = $request->input('input-player8-name');
            $r->player8_phone = $request->input('input-player8-phone');
            $r->player8_fb = $request->input('input-player8-fb');
            $r->player8_email = $request->input('input-player8-email');
            $r->player8_gender = $request->input('radio-player8-gender');
            $r->player8_grade = $request->input('radio-player8-grade');
            $r->player8_id = $request->input('input-player8-id');

            $r->save();

            // $mail = new RegisteredMail($r);
            // if ($r->player1_email != "" && filter_var($r->player1_email, FILTER_VALIDATE_EMAIL)) {
            //     Mail::to($r->player1_email)->queue($mail);
            // }
            // if ($r->player2_email != "" && filter_var($r->player2_email, FILTER_VALIDATE_EMAIL)) {
            //     Mail::to($r->player2_email)->queue($mail);
            // }
            // if ($r->player3_email != "" && filter_var($r->player3_email, FILTER_VALIDATE_EMAIL)) {
            //     Mail::to($r->player3_email)->queue($mail);
            // }
            // if ($r->player4_email != "" && filter_var($r->player4_email, FILTER_VALIDATE_EMAIL)) {
            //     Mail::to($r->player4_email)->queue($mail);
            // }
            // if ($r->player5_email != "" && filter_var($r->player5_email, FILTER_VALIDATE_EMAIL)) {
            //     Mail::to($r->player5_email)->queue($mail);
            // }
            // if ($r->player6_email != "" && filter_var($r->player6_email, FILTER_VALIDATE_EMAIL)) {
            //     Mail::to($r->player6_email)->queue($mail);
            // }
            // if ($r->player7_email != "" && filter_var($r->player7_email, FILTER_VALIDATE_EMAIL)) {
            //     Mail::to($r->player7_email)->queue($mail);
            // }
            // if ($r->player8_email != "" && filter_var($r->player8_email, FILTER_VALIDATE_EMAIL)) {
            //     Mail::to($r->player8_email)->queue($mail);
            // }

        } catch(Exception $e) {
            DB::rollback();

            return back()->withErrors(array('file-image' => 'The file-images field cannot be uploaded.'))->withInput();
        }

        DB::commit();

        return redirect('done-free')->with('status', 'done');
    }

    public function donePage() {
        if (!session('status')) {
            return redirect('register-free');
        }
        return view('done-free');
    }
}
