<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use DB;
use Mail;

use App\Registration2;
use App\Payment;
use App\Mail\RegisteredMail;
use App\Mail\PaymentMail;
use App\Mail\RegisterApprovedMail;

class ConfirmController extends Controller
{
    public function donePage() {
        if (!session('status')) {
            return redirect('confirm');
        }
        return view('confirm-done');
    }

    public function confirmPage($ref = null) {
        $registration2 = null;
        if ($ref != null) {
            $registration2 = Registration2::where('ref', $ref)->first();
        }

        $data = 'https://www.youtube.com/watch?v=DLzxrzFCyOs&t=43s';

        return view('confirm', compact('registration2', 'data'));
    }

    public function confirm(Request $request) {
        $validator = Validator::make($request->all(), [
            'input-ref' => 'required',
            'input-team' => 'required',
            'input-amount' => 'required',
            'input-datetime' => 'required',
            'radio-banks' => 'required',
            'input-from-bank' => 'required',
            'input-from-bank-branch' => 'required',

            'file-image-confirm' => 'image|mimes:jpeg,png,jpg,gif,svg',

        ]);

        if ($validator->fails()) {
            return redirect('confirm')->withErrors($validator)->withInput();
        }

        DB::beginTransaction();
        try {
            $destinationPath = public_path('/uploads/images');

            $image = $request->file('file-image-confirm');
            if ($image) {
                $input['image_confirm'] = time().'image_confirm.'.$image->getClientOriginalExtension();
                $image->move($destinationPath, $input['image_confirm']);
            }
            else {
                $input['image_confirm'] = '';
            }

            $p = new Payment();

            $p->ref = $request->input('input-ref');
            $p->amount = $request->input('input-amount');
            $p->datetime = $request->input('input-datetime');
            $p->to_bank = $request->input('radio-banks');
            $p->from_bank = $request->input('input-from-bank');
            $p->from_bank_branch = $request->input('input-from-bank-branch');
            $p->note = $request->input('input-note');

            $r->image_confirm = $input['image_confirm'] != '' ? 'images/' . $input['image_confirm'] : '';

            $p->save();

            $r = Registration2::where('ref', $p->ref)->first();

            $mail = new PaymentMail($p, $r);
            if ($r->player1_email != "" && filter_var($r->player1_email, FILTER_VALIDATE_EMAIL)) {
                Mail::to($r->player1_email)->queue($mail);
            }
            if ($r->player2_email != "" && filter_var($r->player2_email, FILTER_VALIDATE_EMAIL)) {
                Mail::to($r->player2_email)->queue($mail);
            }
            if ($r->player3_email != "" && filter_var($r->player3_email, FILTER_VALIDATE_EMAIL)) {
                Mail::to($r->player3_email)->queue($mail);
            }
            if ($r->player4_email != "" && filter_var($r->player4_email, FILTER_VALIDATE_EMAIL)) {
                Mail::to($r->player4_email)->queue($mail);
            }
            if ($r->player5_email != "" && filter_var($r->player5_email, FILTER_VALIDATE_EMAIL)) {
                Mail::to($r->player5_email)->queue($mail);
            }
            if ($r->player6_email != "" && filter_var($r->player6_email, FILTER_VALIDATE_EMAIL)) {
                Mail::to($r->player6_email)->queue($mail);
            }
            if ($r->player7_email != "" && filter_var($r->player7_email, FILTER_VALIDATE_EMAIL)) {
                Mail::to($r->player7_email)->queue($mail);
            }
            if ($r->player8_email != "" && filter_var($r->player8_email, FILTER_VALIDATE_EMAIL)) {
                Mail::to($r->player8_email)->queue($mail);
            }

        } catch(Exception $e) {
            DB::rollback();

            return redirect('confirm')->withErrors(array('file-image-confirm' => 'The file-image-confirm field cannot be uploaded.'))->withInput();
        }

        DB::commit();

        return redirect('confirm-done')->with('status', 'done');
    }

    public function getTeams(Request $request) {
        $ref = $request->get('ref');
        $registration2s = Registration2::where('ref', 'like', $ref)->first();

        return $registration2s;
    }
}
