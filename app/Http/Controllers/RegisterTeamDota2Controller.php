<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use DB;
use Mail;

use App\RegistrationDota2;
use App\Mail\Dota2RegisteredMail;

class RegisterTeamDota2Controller extends Controller
{

    public function registerPage() {
        return view('register-d2');
    }

    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'input-team' => 'required',
            'file-image-team' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'file-image-logo' => 'image|mimes:jpeg,png,jpg,gif,svg',

            'input-player1-name' => 'required',
            'input-player1-age' => 'required',
            'input-player1-steam' => 'required',
            'input-player1-phone' => 'required',
            'input-player1-line' => 'required',
            'input-player1-fb' => 'required',
            'input-player1-email' => 'required',
            'input-player1-id' => 'required',

            'input-player2-name' => 'required',
            'input-player2-age' => 'required',
            'input-player2-steam' => 'required',
            'input-player2-id' => 'required',

            'input-player3-name' => 'required',
            'input-player3-age' => 'required',
            'input-player3-steam' => 'required',
            'input-player3-id' => 'required',

            'input-player4-name' => 'required',
            'input-player4-age' => 'required',
            'input-player4-steam' => 'required',
            'input-player4-id' => 'required',

            'input-player5-name' => 'required',
            'input-player5-age' => 'required',
            'input-player5-steam' => 'required',
            'input-player5-id' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('welcome')->withErrors($validator)->withInput();
        }


        DB::beginTransaction();
        try {
            $destinationPath = public_path('/uploads/images');

            $image = $request->file('file-image-team');
            if ($image) {
                $input['image_team_name'] = time().'image_team_name.'.$image->getClientOriginalExtension();
                $image->move($destinationPath, $input['image_team_name']);
            }
            else {
                $input['image_team_name'] = '';
            }

            $image2 = $request->file('file-image-logo');
            if ($image2) {
                $input['image_logo_name'] = time().'image_logo_name.'.$image2->getClientOriginalExtension();
                $image2->move($destinationPath, $input['image_logo_name']);
            }
            else {
                $input['image_logo_name'] = '';
            }

            $r = new RegistrationDota2();
            $r->team = $request->input('input-team');
            $r->image_team = $input['image_team_name'] != '' ? 'images/' . $input['image_team_name'] : '';
            $r->image_logo = $input['image_logo_name'] != '' ? 'images/' . $input['image_logo_name'] : '';

            $r->player1_name = $request->input('input-player1-name');
            $r->player1_age = $request->input('input-player1-age');
            $r->player1_steam = $request->input('input-player1-steam');
            $r->player1_phone = $request->input('input-player1-phone');
            $r->player1_line = $request->input('input-player1-line');
            $r->player1_fb = $request->input('input-player1-fb');
            $r->player1_email = $request->input('input-player1-email');
            $r->player1_id = $request->input('input-player1-id');

            $r->player2_name = $request->input('input-player2-name');
            $r->player2_age = $request->input('input-player2-age');
            $r->player2_steam = $request->input('input-player2-steam');
            $r->player2_id = $request->input('input-player2-id');

            $r->player3_name = $request->input('input-player3-name');
            $r->player3_age = $request->input('input-player3-age');
            $r->player3_steam = $request->input('input-player3-steam');
            $r->player3_id = $request->input('input-player3-id');

            $r->player4_name = $request->input('input-player4-name');
            $r->player4_age = $request->input('input-player4-age');
            $r->player4_steam = $request->input('input-player4-steam');
            $r->player4_id = $request->input('input-player4-id');

            $r->player5_name = $request->input('input-player5-name');
            $r->player5_age = $request->input('input-player5-age');
            $r->player5_steam = $request->input('input-player5-steam');
            $r->player5_id = $request->input('input-player5-id');

            $r->player6_name = $request->input('input-player6-name');
            $r->player6_age = $request->input('input-player6-age');
            $r->player6_steam = $request->input('input-player6-steam');
            $r->player6_id = $request->input('input-player6-id');

            $r->player7_name = $request->input('input-player7-name');
            $r->player7_age = $request->input('input-player7-age');
            $r->player7_steam = $request->input('input-player7-steam');
            $r->player7_id = $request->input('input-player7-id');

            $r->player8_name = $request->input('input-player8-name');
            $r->player8_age = $request->input('input-player8-age');
            $r->player8_steam = $request->input('input-player8-steam');
            $r->player8_id = $request->input('input-player8-id');

            $r->save();

            // $mail = new Dota2RegisteredMail($r);
            // if ($r->player1_email != "" && filter_var($r->player1_email, FILTER_VALIDATE_EMAIL)) {
            //     Mail::to($r->player1_email)->queue($mail);
            // }
            // if ($r->player2_email != "" && filter_var($r->player2_email, FILTER_VALIDATE_EMAIL)) {
            //     Mail::to($r->player2_email)->queue($mail);
            // }
            // if ($r->player3_email != "" && filter_var($r->player3_email, FILTER_VALIDATE_EMAIL)) {
            //     Mail::to($r->player3_email)->queue($mail);
            // }
            // if ($r->player4_email != "" && filter_var($r->player4_email, FILTER_VALIDATE_EMAIL)) {
            //     Mail::to($r->player4_email)->queue($mail);
            // }
            // if ($r->player5_email != "" && filter_var($r->player5_email, FILTER_VALIDATE_EMAIL)) {
            //     Mail::to($r->player5_email)->queue($mail);
            // }
            // if ($r->player6_email != "" && filter_var($r->player6_email, FILTER_VALIDATE_EMAIL)) {
            //     Mail::to($r->player6_email)->queue($mail);
            // }
            // if ($r->player7_email != "" && filter_var($r->player7_email, FILTER_VALIDATE_EMAIL)) {
            //     Mail::to($r->player7_email)->queue($mail);
            // }
            // if ($r->player8_email != "" && filter_var($r->player8_email, FILTER_VALIDATE_EMAIL)) {
            //     Mail::to($r->player8_email)->queue($mail);
            // }

        } catch(Exception $e) {
            DB::rollback();

            return back()->withErrors(array('file-image' => 'The file-images field cannot be uploaded.'))->withInput();
        }

        DB::commit();

        return redirect('done-d2')->with('status', 'done');
    }

    public function donePage() {
        // if (!session('status')) {
        //     return redirect('register-d2');
        // }
        return view('done-d2');
    }
}
