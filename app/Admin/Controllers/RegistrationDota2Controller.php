<?php

namespace App\Admin\Controllers;

use App\RegistrationDota2;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class RegistrationDota2Controller extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(RegistrationDota2::class, function (Grid $grid) {

            $grid->id('ID')->sortable();

            $grid->column('image_team', 'Team Image')->display(function ($path) {
                return 'http://otc.bkkdeveloper.com/uploads/'. $path;
            })->image();

            $grid->column('image_logo', 'Team Logo')->display(function ($path) {
                return 'http://otc.bkkdeveloper.com/uploads/'. $path;
            })->image();
            $grid->column('team', 'Team Name');
            $grid->column('approved', 'Status')->display(function ($status) {
                return $status ? 'อนุมัติแล้ว' : '';
            });
            $grid->column('player1_name', 'Player 1 Name');
            $grid->column('player1_age', 'Player 1 Age');
            $grid->column('player1_steam', 'Player 1 Steam ID');
            $grid->column('player1_phone', 'Player 1 Phone');
            $grid->column('player1_line', 'Player 1 Line ID');
            $grid->column('player1_fb', 'Player 1 Facebook');
            $grid->column('player1_email', 'Player 1 Email');

            $grid->created_at()->sortable();
            $grid->updated_at()->sortable();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(RegistrationDota2::class, function (Form $form) {
            $form->tab('ข้อมูลทั่วไป', function ($form) {

                $form->display('id', 'ID');

                $form->display('team', 'Team Name');
                $form->image('image_team', 'Team Image')->name(function ($path) {
                    return 'https://otc.bkkdeveloper.com/'. $path;
                });
                $form->image('image_logo', 'Team Logo')->name(function ($path) {
                    return 'https://otc.bkkdeveloper.com/'. $path;
                });

                $form->display('created_at', 'Created At');
                $form->display('updated_at', 'Updated At');

            })->tab('สถานะการอนุมัติ', function ($form) {
                $form->image('image_receipt', 'Receipt')->name(function ($path) {
                    return 'https://otc.bkkdeveloper.com/'. $path;
                });

                $form->switch('approved', 'Approved');

            })->tab('คนที่ 1 (หัวหน้าทีม)', function ($form) {
                $form->text('player1_name');
                $form->number('player1_age');
                $form->text('player1_steam');
                $form->text('player1_phone');
                $form->text('player1_line');
                $form->text('player1_fb');
                $form->text('player1_email');
                $form->text('player1_id');

            })->tab('คนที่ 2', function ($form) {
                $form->text('player2_name');
                $form->number('player2_age');
                $form->text('player2_steam');
                $form->text('player2_id');

            })->tab('คนที่ 3', function ($form) {
                $form->text('player3_name');
                $form->number('player3_age');
                $form->text('player3_steam');
                $form->text('player3_id');

            })->tab('คนที่ 4', function ($form) {
                $form->text('player4_name');
                $form->number('player4_age');
                $form->text('player4_steam');
                $form->text('player4_id');

            })->tab('คนที่ 5', function ($form) {
                $form->text('player5_name');
                $form->number('player5_age');
                $form->text('player5_steam');
                $form->text('player5_id');

            })->tab('คนที่ 6', function ($form) {
                $form->text('player6_name');
                $form->number('player6_age');
                $form->text('player6_steam');
                $form->text('player6_id');

            })->tab('คนที่ 7', function ($form) {
                $form->text('player7_name');
                $form->number('player7_age');
                $form->text('player7_steam');
                $form->text('player7_id');

            })->tab('คนที่ 8', function ($form) {
                $form->text('player8_name');
                $form->number('player8_age');
                $form->text('player8_steam');
                $form->text('player8_id');

            });
        });
    }
}
