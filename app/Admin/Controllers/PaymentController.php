<?php

namespace App\Admin\Controllers;

use Mail;

use App\Payment;
use App\Registration2;

use App\Mail\RegisterApprovedMail;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

use Illuminate\Support\MessageBag;

class PaymentController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Payment');
            $content->description('สำหรับจัดการรายการแจ้งชำระเงิน');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Payment');
            $content->description('ดูรายละเอียด และแก้ไขข้อมูล');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Payment');
            $content->description('เพิ่มการชำระเงิน ไม่แนะนำให้ admin เพิ่มจากหน้านี้ ให้แจ้งชำระเหมือนน้องๆ นะครับ');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Payment::class, function (Grid $grid) {

            $grid->id('ID')->sortable();

            $grid->column('ref', 'Reference Number')->sortable();
            $grid->column('amount', 'Amount');
            $grid->column('datetime', 'Transfer Date Time')->sortable();
            $grid->column('to_bank', 'To Bank');
            $grid->column('image_confirm', 'Image Confirm')->display(function ($path) {
                return 'https://otc.bkkdeveloper.com/uploads/'. $path;
            })->image();

            $grid->column('confirmed', 'Confirmed')->display(function ($value) {
                if ($value) {
                    return "<span class='label label-success'>Confirmed</span>";
                }
                else {
                    return "<span class='label label-warning'>Pending</span>";
                }
            });

            $grid->registration2('Registration Team')->display(function ($registration2) {
                return $registration2['team'];
            });

            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Payment::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->display('ref');
            $form->display('amount');
            $form->display('datetime');
            $form->display('to_bank');
            $form->display('from_bank');
            $form->display('from_bank_branch');
            $form->display('note');

            $form->image('image_confirm')->name(function ($path) {
                // echo 'https://otc.bkkdeveloper.com/'. $path;
                // return 111;
                return 'https://otc.bkkdeveloper.com/'. $path;
            });

            $form->switch('confirmed', 'Confirmed');


            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');


            $form->saved(function (Form $form) {

                if ($form->model()->confirmed) {
                    $r = Registration2::where('ref', $form->model()->ref)->first();
                    $p = Payment::where('id', $form->model()->id)->first();

                    $mail = new RegisterApprovedMail($p, $r);
                    if ($r->player1_email != "" && filter_var($r->player1_email, FILTER_VALIDATE_EMAIL)) {
                        Mail::to($r->player1_email)->queue($mail);
                    }
                    if ($r->player2_email != "" && filter_var($r->player2_email, FILTER_VALIDATE_EMAIL)) {
                        Mail::to($r->player2_email)->queue($mail);
                    }
                    if ($r->player3_email != "" && filter_var($r->player3_email, FILTER_VALIDATE_EMAIL)) {
                        Mail::to($r->player3_email)->queue($mail);
                    }
                    if ($r->player4_email != "" && filter_var($r->player4_email, FILTER_VALIDATE_EMAIL)) {
                        Mail::to($r->player4_email)->queue($mail);
                    }
                    if ($r->player5_email != "" && filter_var($r->player5_email, FILTER_VALIDATE_EMAIL)) {
                        Mail::to($r->player5_email)->queue($mail);
                    }
                    if ($r->player6_email != "" && filter_var($r->player6_email, FILTER_VALIDATE_EMAIL)) {
                        Mail::to($r->player6_email)->queue($mail);
                    }
                    if ($r->player7_email != "" && filter_var($r->player7_email, FILTER_VALIDATE_EMAIL)) {
                        Mail::to($r->player7_email)->queue($mail);
                    }
                    if ($r->player8_email != "" && filter_var($r->player8_email, FILTER_VALIDATE_EMAIL)) {
                        Mail::to($r->player8_email)->queue($mail);
                    }

                }

                $success = new MessageBag([
                    'title'   => 'Updated',
                    'message' => 'Update succeeded !',
                ]);

                return back()->with(compact('success'));

            });

        });
    }
}
