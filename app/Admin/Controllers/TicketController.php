<?php

namespace App\Admin\Controllers;

use App\Ticket;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

use Illuminate\Support\MessageBag;

class TicketController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Ticket::class, function (Grid $grid) {

            $grid->id('ID')->sortable();

            $grid->column('code', 'Code');
            $grid->column('url', 'URL')->display(function($url) {
                return 'http://otc.bkkdeveloper.com/register/ticket/' . $url;
            });
            $grid->column('note', 'Note');

            $grid->column('registration22.team', 'Registration')->display(function ($team) {
                return $team;
            });

            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Ticket::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->display('code', 'Code');
            $form->display('url', 'URL');
            $form->text('note', 'Note');

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');


            $form->saved(function (Form $form) {

                if ($form->model()->id != null) {

                    $t = $form->model();
                    $t->code = $t->generateCode();
                    $t->url = $t->generateUrl();
                    $t->save();

                }

                $success = new MessageBag([
                    'title'   => 'สร้าง Ticket เรียบร้อยแล้ว',
                    'message' => 'สามารถส่ง url เพื่อทำการสมัครได้เลยครับ',
                ]);

                // return back()->with(compact('success'));

            });



        });
    }
}
