<?php

namespace App\Admin\Controllers;

use App\Registration2;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

// use App\Admin\Extensions\ExcelExpoter;


class Registration2Controller extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Registration');
            $content->description('สำหรับจัดการรายการทีมที่สมัคร');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Registration');
            $content->description('ดูรายละเอียด และแก้ไขข้อมูล (โปรดระวัง เมื่อกด Save แล้ว ข้อมูลเก่าจะถูกข้อมูลใหม่บันทึกทับลงไปทันที)');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Registration');
            $content->description('ไม่แนะนำให้สร้างนะ ให้เด็กสมัครเองดีกว่าครับ');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Registration2::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->column('school', 'School')->sortable();
            $grid->column('team', 'Team')->sortable();

            // $grid->column('image_team')->display(function ($images) {
            //     return json_decode($images, true);
            //
            // })->map(function ($path) {
            //
            //     return 'https://otc.bkkdeveloper.com/'. $path;
            //
            // })->image();

            $grid->column('image_team', 'Team Image')->display(function ($path) {
                return 'https://otc.bkkdeveloper.com/uploads/'. $path;
            })->image();

            $grid->column('image_logo', 'Team Logo')->display(function ($path) {
                return 'https://otc.bkkdeveloper.com/uploads/'. $path;
            })->image();

            $grid->payments('Payment')->display(function ($payments) {
                if ($payments == null) {
                    return "";
                }
                else {
                    if ($payments['confirmed'] == 1) {
                        return "<span class='label label-success'>Confirmed</span>";
                    }
                    else {
                        return "<span class='label label-warning'>Pending</span>";
                    }
                }
            });

            $grid->column('player1_name', 'Player 1 Name');
            $grid->column('player1_phone', 'Player 1 Phone');
            $grid->column('player1_line', 'Player 1 Line ID');
            $grid->column('player1_fb', 'Player 1 Facebook');
            $grid->column('player1_email', 'Player 1 Email');

            $grid->created_at()->sortable();
            $grid->updated_at()->sortable();

            // $grid->exporter(new ExcelExpoter());
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Registration2::class, function (Form $form) {

            $genders = [
                'male' => 'ชาย',
                'female' => 'หญิง'
            ];

            $grades = [
                1 => 'ม.1',
                2 => 'ม.2',
                3 => 'ม.3',
                4 => 'ม.4',
                5 => 'ม.5',
                6 => 'ม.6'
            ];

            $form->display('id', 'ID');

            $form->display('ref');
            $form->display('school', 'School');
            $form->display('team', 'Team Name');
            $form->image('image_team', 'Team Image');
            $form->image('image_logo', 'Team Logo')->name(function ($path) {
                return 'https://otc.bkkdeveloper.com/'. $path;
            });

            $form->text('player1_name');
            $form->text('player1_phone');
            $form->text('player1_line');
            $form->text('player1_fb');
            $form->text('player1_email');
            // $form->display('player1_gender');
            // $form->display('player1_grade');
            $form->select('player1_gender', 'Player 1 Gender')->options($genders);
            $form->select('player1_grade', 'Player 1 Grade')->options($grades);
            $form->text('player1_id');

            $form->text('player2_name');
            $form->text('player2_phone');
            $form->text('player2_fb');
            $form->text('player2_email');
            // $form->display('player2_gender');
            // $form->display('player2_grade');
            $form->select('player2_gender', 'Player 2 Gender')->options($genders);
            $form->select('player2_grade', 'Player 2 Grade')->options($grades);
            $form->text('player2_id');

            $form->text('player3_name');
            $form->text('player3_phone');
            $form->text('player3_fb');
            $form->text('player3_email');
            // $form->display('player3_gender');
            // $form->display('player3_grade');
            $form->select('player3_gender', 'Player 3 Gender')->options($genders);
            $form->select('player3_grade', 'Player 3 Grade')->options($grades);
            $form->text('player3_id');

            $form->text('player4_name');
            $form->text('player4_phone');
            $form->text('player4_fb');
            $form->text('player4_email');
            // $form->display('player4_gender');
            // $form->display('player4_grade');
            $form->select('player4_gender', 'Player 4 Gender')->options($genders);
            $form->select('player4_grade', 'Player 4 Grade')->options($grades);
            $form->text('player4_id');

            $form->text('player5_name');
            $form->text('player5_phone');
            $form->text('player5_fb');
            $form->text('player5_email');
            // $form->display('player5_gender');
            // $form->display('player5_grade');
            $form->select('player5_gender', 'Player 5 Gender')->options($genders);
            $form->select('player5_grade', 'Player 5 Grade')->options($grades);
            $form->text('player5_id');

            $form->text('player6_name');
            $form->text('player6_phone');
            $form->text('player6_fb');
            $form->text('player6_email');
            // $form->display('player6_gender');
            // $form->display('player6_grade');
            $form->select('player6_gender', 'Player 6 Gender')->options($genders);
            $form->select('player6_grade', 'Player 6 Grade')->options($grades);
            $form->text('player6_id');

            $form->text('player7_name');
            $form->text('player7_phone');
            $form->text('player7_fb');
            $form->text('player7_email');
            // $form->display('player7_gender');
            // $form->display('player7_grade');
            $form->select('player7_gender', 'Player 7 Gender')->options($genders);
            $form->select('player7_grade', 'Player 7 Grade')->options($grades);
            $form->text('player7_id');

            $form->text('player8_name');
            $form->text('player8_phone');
            $form->text('player8_fb');
            $form->text('player8_email');
            // $form->display('player8_gender');
            // $form->display('player8_grade');
            $form->select('player8_gender', 'Player 8 Gender')->options($genders);
            $form->select('player8_grade', 'Player 8 Grade')->options($grades);
            $form->text('player8_id');



            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
}
