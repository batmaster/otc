<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');

    // $router->resource('demo/registrations', Registration2Controller::class);
    // $router->resource('demo/payments', PaymentController::class);

    $router->resource('demo/tickets', TicketController::class);

    $router->resource('demo/registrations', Registration22Controller::class);

    $router->resource('demo/registration-d2s', RegistrationDota2Controller::class);

    $router->resource('demo/registration-frees', RegistrationFreeController::class);

});
