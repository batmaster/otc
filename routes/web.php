<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});

Route::get('welcome', function () {
    return view('welcome');
});

Route::get('notice', function () {
    return view('notice');
});

Route::get('rule', function () {
    return view('rule');
});



Route::get('register/ticket/{u?}', 'RegisterTeamController@registerPage');
Route::post('registered', 'RegisterTeamController@register');
Route::get('done', 'RegisterTeamController@donePage');

Route::get('confirm/{ref?}', 'ConfirmController@confirmPage');
Route::get('teams', 'ConfirmController@getTeams');
Route::post('confirm', 'ConfirmController@confirm');
Route::get('confirm-done', 'ConfirmController@donePage');

Route::get('register-d2', 'RegisterTeamDota2Controller@registerPage');
Route::post('registered-d2', 'RegisterTeamDota2Controller@register');
Route::get('done-d2', 'RegisterTeamDota2Controller@donePage');
Route::get('notice-d2', function () {
    return view('notice-d2');
});
Route::get('rule-d2', function () {
    return view('rule-d2');
});
Route::get('how-to-get-steam', function () {
    return view('how-to-get-steam');
});


Route::get('register-free', 'RegisterTeamFreeController@registerPage');
Route::post('registered-free', 'RegisterTeamFreeController@register');
Route::get('done-free', 'RegisterTeamFreeController@donePage');


Route::get('redirect-line', function () {
    $og = new ChrisKonnertz\OpenGraph\OpenGraph();

    $og->title('OTW eSports | โครงการการแข่งขันกีฬาอิเล็กทรอนิกส์สำหรับเยาวชนไทย')
        ->type('website')
        ->image('http://otc.bkkdeveloper.com/assets/images/redirect-line-bg.png')
        ->description('โครงการการแข่งขันกีฬา eSports สำหรับเยาวชนไทยรุ่นใหม่ ในระดับชั้นมัธยมศึกษาหรือเทียบเท่า')
        ->url('http://otc.bkkdeveloper.com/redirect-line');

    return view('redirect-line', compact('og'));
});

// Auth::routes();

// Route::get('admin/login', 'Auth\LoginController@showLoginForm')->name('login');
// Route::post('admin/login', 'Auth\LoginController@login');
// Route::post('admin/logout', 'Auth\LoginController@logout')->name('logout');


// Route::get('admin', 'HomeController@index');
// Route::get('admin/home', 'HomeController@index')->name('home');
